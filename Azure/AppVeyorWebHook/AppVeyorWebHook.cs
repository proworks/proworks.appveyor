using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Threading;

namespace ProWorks.AppVeyor
{
    public static class AppVeyorWebHook
    {
        private static Dictionary<string, string> _knownAppVeyorUrls = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            ["nowpow/simon"] = "https://ci.appveyor.com/api/git/webhook?id=i6vytoliiqkr1s2j",
            ["USDAERS/usda.umbraco"] = "https://ci.appveyor.com/api/git/webhook?id=q4f5uvxqb99rfvbj",
            ["preferredhotels/preferrednetumbraco"] = "https://ci.appveyor.com/api/git/webhook?id=d861wljhe3i6dtbd"
        };

        [FunctionName("AppVeyorWebHook")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req,
            ILogger log,
            CancellationToken cancellationToken)
        {
            var body = await req.ReadAsStringAsync().ConfigureAwait(false);

            if (string.IsNullOrWhiteSpace(body) || body.Trim()[0] != '{') return Error(log, "Invalid request - {body}", body).Result as IActionResult;

            try
            {
                var data = JObject.Parse(body);
                var (result, repositoryKey) = data?["resource"]?["repository"] != null
                    ? AzureDevOpsResult(log, data)
                    : (data?["repository"] != null
                        ? BitBucketResult(log, data)
                        : Error(log, "Unknown request - {body}", body));

                if (result is JObject webhookData)
                {
                    if (!Uri.TryCreate(req.Headers["x-appveyor-url"].ToString(), UriKind.Absolute, out var appVeyorUrl)
                        && !Uri.TryCreate(_knownAppVeyorUrls.TryGetValue(repositoryKey, out var triggerUrl) ? triggerUrl : "", UriKind.Absolute, out appVeyorUrl))
                    {
                        return Error(log, "Invalid x-appveyor-url header or no known repository").Result as IActionResult;
                    }

                    if (req.Query["log-message"].ToString() == "true")
                    {
                        log.LogInformation("Sending message to {url} - {message}", appVeyorUrl, webhookData);
                    }

                    using (var client = new HttpClient())
                    {
                        var response = await client.PostAsJsonAsync(appVeyorUrl, webhookData, cancellationToken);
                        if (response.IsSuccessStatusCode)
                        {
                            log.LogInformation("Got success response - {status}", response.StatusCode.ToString());
                        }
                        else
                        {
                            body = await response.Content.ReadAsStringAsync();
                            log.LogWarning("Got error response - {status} - {body}", response.StatusCode.ToString(), body);
                        }
                    }

                    return new OkResult();
                }
                else return result as IActionResult;
            }
            catch (Exception ex)
            {
                return Error(log, ex, "Could not process request - {body}", body).Result as IActionResult;
            }
        }

        private static (object Result, string RepositoryKey) Error(ILogger log, string message, params object[] args)
        {
                log.LogError(message, args);
                return (new BadRequestResult(), null);
        }

        private static (object Result, string RepositoryKey) Error(ILogger log, Exception ex, string message, params object[] args)
        {
                log.LogError(ex, message, args);
                return (new BadRequestResult(), null);
        }

        private static (object Result, string RepositoryKey) BitBucketResult(ILogger log, JObject data)
        {
                if (!(data["push"] is JObject push)) return Error(log, "Missing push property");
                if (!(push["changes"] is JArray changes)) return Error(log, "Missing changes property");
                if (!(changes.Count > 0 && changes[0] is JObject change)) return Error(log, "Missing change property");
                if (!(change["commits"] is JArray commits)) return Error(log, "Missing commits property");
                if (!(commits.Count > 0 && commits[0] is JObject commit)) return Error(log, "Missing commit property");
                if (!(data["actor"] is JObject actor)) return Error(log, "Missing actor property");
                if (!(data["repository"] is JObject repository)) return Error(log, "Missing repository property");

                var repoKey = repository["full_name"]?.ToString();
                var actorEmail = actor["nickname"]?.ToString() ?? actor["username"]?.ToString() ?? "";
                var domain = "users.noreply.bitbucket.com";
                var idx = actorEmail.IndexOf('\\');
                if (idx >= 0) {
                    domain = actorEmail.Substring(0, idx);
                    actorEmail = actorEmail.Substring(idx + 1);
                }
                if (!actorEmail.Contains('@')) {
                    actorEmail += '@' + domain;
                }

                var comment = commit["summary"]?["raw"]?.ToString();
                var commitId = commit["hash"]?.ToString();
                var authorName = commit["author"]?["user"]?["display_name"]?.ToString();
                var authorEmail = commit["author"]?["user"]?["nickname"]?.ToString();
                var branch = change["new"]?["name"]?.ToString() ?? change["old"]?["name"]?.ToString() ?? "main";

                idx = authorEmail.IndexOf('\\');
                if (idx >= 0) {
                    domain = authorEmail.Substring(0, idx);
                    authorEmail = authorEmail.Substring(idx + 1);
                }
                if (!authorEmail.Contains('@')) {
                    authorEmail += '@' + domain;
                }

                log.LogInformation("Triggering an AppVeyor webhook call to {uri} for commit '{commitMessage}' (# {commitId}) on branch {branch}", repoKey, comment, commitId, branch);

                var webhookData = new JObject(
                    new JProperty("ref", $"refs/heads/{branch}"),
                    new JProperty("repository", new JObject(
                        new JProperty("name", repository["name"]),
                        new JProperty("full_name", repository["full_name"]),
                        new JProperty("html_url", repository["links"]?["html"]?["href"]),
                        new JProperty("url", repository["links"]?["html"]?["href"]),
                        new JProperty("owner", new JObject(
                            new JProperty("name", repository["owner"]?["display_name"]),
                            new JProperty("email", (repository["owner"]?["nickname"] ?? repository["owner"]?["username"]) + "@" + domain),
                            new JProperty("login", repository["owner"]?["nickname"] ?? repository["owner"]?["username"]),
                            new JProperty("avatar_url", repository["owner"]?["links"]?["avatar"]?["href"]),
                            new JProperty("url", repository["owner"]?["links"]?["html"]?["href"]),
                            new JProperty("html_url", repository["owner"]?["links"]?["html"]?["href"])
                        ))
                    )),
                    new JProperty("pusher", new JObject(
                        new JProperty("name", actor["display_name"]),
                        new JProperty("email", actorEmail)
                    )),
                    new JProperty("sender", new JObject(
                        new JProperty("name", authorName),
                        new JProperty("email", authorEmail),
                        new JProperty("login", authorEmail),
                        new JProperty("avatar_url", commit["author"]?["user"]?["links"]?["avatar"]?["href"]),
                        new JProperty("url", commit["author"]?["user"]?["links"]?["html"]?["href"]),
                        new JProperty("html_url", commit["author"]?["user"]?["links"]?["html"]?["href"])
                    )),
                    new JProperty("compare", change["links"]?["html"]?["href"]),
                    new JProperty("commits", new JArray(commits.Select(MapBitBucketCommit).ToArray()))
                );

                if (change["old"] is JObject old)
                {
                    webhookData.Add("before", old["target"]?["hash"]);
                }

                if (change["new"] is JObject update)
                {
                    webhookData.Add("after", update["target"]?["hash"]);
                    webhookData.Add("head_commit", MapBitBucketCommit(commit));
                }

            return (webhookData, repoKey);
        }

        private static JObject MapBitBucketCommit(JToken commit) => new JObject(
            new JProperty("id", commit["hash"]),
            new JProperty("tree_id", commit["parents"] is JArray parents && parents.Count > 0 ? parents[0]?["hash"] : commit["hash"]),
            new JProperty("distinct", true),
            new JProperty("message", commit["message"]),
            new JProperty("timestamp", commit["date"]),
            new JProperty("url", commit["links"]?["html"]?["href"]),
            new JProperty("author", new JObject(
                new JProperty("name", commit["author"]?["user"]?["display_name"]),
                new JProperty("email", (commit["author"]?["user"]?["nickname"] ?? commit["author"]?["user"]?["username"]) + "@users.noreply.bitbucket.com"),
                new JProperty("username", commit["author"]?["user"]?["nickname"])
            )),
            new JProperty("committer", new JObject(
                new JProperty("name", "GitHub"),
                new JProperty("email", "noreply@github.com"),
                new JProperty("username", "web-flow")
            )),
            new JProperty("added", new JArray()),
            new JProperty("removed", new JArray()),
            new JProperty("modified", new JArray())
        );

        private static (object Result, string RepositoryKey) AzureDevOpsResult(ILogger log, JObject data)
        {
                if (data["eventType"]?.ToString() != "git.push") return Error(log, "Invalid event type {type}", data["eventType"]?.ToString());
                if (!(data["resource"] is JObject resource)) return Error(log, "Missing resource property");
                if (!(resource["refUpdates"] is JArray updates)) return Error(log, "Missing refUpdates property");
                if (!(updates.Count > 0 && updates[0] is JObject update)) return Error(log, "Missing update property");
                if (!(resource["pushedBy"] is JObject pushedBy)) return Error(log, "Missing pushedBy property");
                if (!(resource["repository"] is JObject repository)) return Error(log, "Missing repository property");

                var pushedByEmail = pushedBy["uniqueName"]?.ToString() ?? "";
                var domain = "dev.azure.com";
                var idx = pushedByEmail.IndexOf('\\');
                if (idx >= 0) {
                    domain = pushedByEmail.Substring(0, idx);
                    pushedByEmail = pushedByEmail.Substring(idx + 1);
                }
                if (!pushedByEmail.Contains('@')) {
                    pushedByEmail += '@' + domain;
                }

                var repoKey = repository["url"]?.ToString();
                var comment = data["message"]?["text"]?.ToString();
                var commitId = "pushId#" + resource["pushId"]?.ToString();
                var commitUrl = resource["url"]?.ToString();
                var timestamp = resource["date"]?.ToString();
                var authorName = pushedBy["displayName"]?.ToString();
                var authorEmail = pushedByEmail;

                if (resource["commits"] is JArray commits && commits.Count > 0 && commits[0] is JObject commit && commit["author"] is JObject author)
                {
                    comment = commit["comment"]?.ToString() ?? comment;
                    commitId = commit["commitId"]?.ToString() ?? commitId;
                    commitUrl = commit["url"]?.ToString() ?? commitUrl;
                    timestamp = author["date"]?.ToString() ?? timestamp;
                    authorName = author["name"]?.ToString() ?? authorName;
                    authorEmail = author["email"]?.ToString() ?? authorEmail;
                }

                log.LogInformation("Triggering an AppVeyor webhook call to {uri} for commit '{commitMessage}' (# {commitId}) on branch {branch}", repoKey, comment, commitId, update["name"]?.ToString());

                var webhookData = new JObject(
                    new JProperty("ref", update["name"]),
                    new JProperty("before", update["oldObjectId"]),
                    new JProperty("after", update["newObjectId"]),
                    new JProperty("head_commit", new JObject(
                        new JProperty("id", update["newObjectId"]),
                        new JProperty("message", comment),
                        new JProperty("timestamp", timestamp),
                        new JProperty("url", commitUrl),
                        new JProperty("author", new JObject(
                            new JProperty("name", authorName),
                            new JProperty("email", authorEmail),
                            new JProperty("username", authorEmail)
                        ))
                    )),
                    new JProperty("repository", new JObject(
                        new JProperty("name", repository["name"]),
                        new JProperty("full_name", repository["name"]),
                        new JProperty("html_url", repository["remoteUrl"]),
                        new JProperty("url", repository["url"]),
                        new JProperty("sender", new JObject(
                            new JProperty("name", authorName),
                            new JProperty("email", authorEmail),
                            new JProperty("login", authorEmail)
                        ))
                    )),
                    new JProperty("pusher", new JObject(
                        new JProperty("name", pushedBy["displayName"]),
                        new JProperty("email", pushedByEmail)
                    )),
                    new JProperty("sender", new JObject(
                        new JProperty("login", pushedByEmail)
                    ))
                );

            return (webhookData, repoKey);
        }
    }
}
