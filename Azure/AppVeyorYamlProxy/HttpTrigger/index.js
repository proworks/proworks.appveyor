const https = require('https')
const bbUser = process.env.BIT_BUCKET_USERNAME; // Using "proworks-dev" account
const bbPass = process.env.BIT_BUCKET_PASSWORD; // Custom app password
const bbHeaders = [
    'Authorization', 'Basic ' + new Buffer(bbUser + ':' + bbPass).toString('base64'),
    'Accept', 'application/json'
]
const YamlBaseUrl = 'https://api.bitbucket.org/2.0/repositories/proworks/proworks.appveyor.yaml/src/master/'

module.exports = async function (context, req) {

    context.log('###############################################')
    context.log('Got request at ' + new Date())
    context.log(req.method + ' ' + req.url)

    var requestPromise = new Promise((resolve, reject) => {
        var url = YamlBaseUrl + req.query.name + (/\.[Yy][Mm][Ll]$/.test(req.query.name) ? '' : '.yml')
        context.log('Retrieving YAML from ' + url)
        var reqYaml = https.request(url, (res) => {
            yamlStr = ""
            var dataLength = 0
            res.on('data', (chunk) => {
                yamlStr += chunk
                dataLength += chunk.length
            })
            res.on('end', () => {
                if (dataLength === 0 || res.statusCode !== 200) {
                    context.log('Got ' + res.statusCode + ' - ' + res.statusMessage)
                    reject('Got ' + res.statusCode + ' - ' + res.statusMessage)
                    for(var i = 0; i < (res.rawHeaders.length - 1); i += 2) {
                        context.log(res.rawHeaders[i] + ': ' + res.rawHeaders[i + 1])
                    }
                } else {
                    context.log('Wrote ' + dataLength + ' bytes to response stream')
                    resolve(yamlStr)
                }
            })
        })
        reqYaml.on('error', (e) => {
            context.error('ERROR: ' + e)
            reject(e)
        })
    
        for (var i = 0; i < (bbHeaders.length - 1); i += 2) {
            reqYaml.setHeader(bbHeaders[i], bbHeaders[i + 1])
        }
        reqYaml.end()
    })

    await requestPromise.then(
        function(yamlStr) {
            context.res = {
                body: yamlStr,
                isRaw: true
            }
        },
        function(e) {
            context.res = {
                status: 400,
                body: e
            }
        })
}