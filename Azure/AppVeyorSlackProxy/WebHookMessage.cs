using Newtonsoft.Json;

namespace ProWorks.AppVeyor.SlackProxy
{
    public class WebHookMessage
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool Success { get; set; }
        public string CommitUrl { get; set; }
        public string CommitId { get; set; }
        public string CommitAuthor { get; set; }
        public string CommitAuthorEmail { get; set; }
        public string CommitDate { get; set; }
        public string CommitMessage { get; set; }
    }
}