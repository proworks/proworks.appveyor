using Newtonsoft.Json.Linq;

namespace ProWorks.AppVeyor.SlackProxy
{
    public class EnvironmentData
    {
        public long EnvironmentId { get; set; }
        public string EnvironmentUrl { get; set; }
        public string Name { get; set; }
        public string Provider { get; set; }
    }
}