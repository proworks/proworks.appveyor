using Newtonsoft.Json.Linq;

namespace ProWorks.AppVeyor.SlackProxy
{
    public class DeploymentData : Runnable
    {
        public string Name => $"of {Build?.Name} to {Environment?.Name}";
        public long DeploymentId { get; set; }
        public string DeploymentUrl { get; set; }
        public string NotificationSettingsUrl { get; set; }
        public JArray Jobs { get; set; }
        public EnvironmentData Environment { get; set; }
        public BuildData Build { get; set; }
        public BuildJobData BuildJob { get; set; }

        public WebHookMessage ToMessage()
        {
            var msg = Build.ToMessage();

            msg.Type = "Deployment";
            msg.Name = Name;
            msg.Url = DeploymentUrl;
            msg.Success = Passed;

            return msg;
        }
    }
}