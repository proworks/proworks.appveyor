using Newtonsoft.Json.Linq;

namespace ProWorks.AppVeyor.SlackProxy
{
    public class BuildJobData : Runnable
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public long MessagesTotal { get; set; }
        public long MessagesError { get; set; }
        public CompilationMessage[] CompilationMessages { get; set; }
        public Artifact[] Artifacts { get; set; }
    }
}