using Newtonsoft.Json.Linq;

namespace ProWorks.AppVeyor.SlackProxy
{
    public class Artifact
    {
        public string Permalink { get; set; }
        public string FileName { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public long Size { get; set; }
        public string Url { get; set; }
    }
}