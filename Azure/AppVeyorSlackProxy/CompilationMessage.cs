using Newtonsoft.Json.Linq;

namespace ProWorks.AppVeyor.SlackProxy
{
    public class CompilationMessage
    {
        public string Category { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public string FileName { get; set; }
        public long Line { get; set; }
        public long Column { get; set; }
        public string ProjectName { get; set; }
        public string ProjectFileName { get; set; }
        public string Created { get; set; }
    }
}