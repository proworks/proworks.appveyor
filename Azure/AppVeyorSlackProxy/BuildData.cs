using Newtonsoft.Json.Linq;

namespace ProWorks.AppVeyor.SlackProxy
{
    public class BuildData : Runnable
    {
        public string Name => $"{ProjectName} {BuildVersion}";
        public long ProjectId { get; set; }
        public string ProjectName { get; set; }
        public long BuildId { get; set; }
        public long BuildNumber { get; set; }
        public string BuildVersion { get; set; }
        public string RepositoryProvider { get; set; }
        public string RepositoryScm { get; set; }
        public string RepositoryName { get; set; }
        public string Branch { get; set; }
        public string CommitId { get; set; }
        public string CommitAuthor { get; set; }
        public string CommitAuthorEmail { get; set; }
        public string CommitAuthorUsername { get; set; }
        public string CommitDate { get; set; }
        public string CommitMessage { get; set; }
        public string CommitMessageExtended { get; set; }
        public string CommitterName { get; set; }
        public string CommitterEmail { get; set; }
        public string BuildUrl { get; set; }
        public string CommitUrl { get; set; }
        public string NotificationSettingsUrl { get; set; }
        public bool IsPullRequest { get; set; }
        public BuildJobData[] Jobs { get; set; }

        public WebHookMessage ToMessage() => new WebHookMessage
            {
                Type = "Build",
                Name = Name,
                Url = BuildUrl,
                Success = Passed,
                CommitId = CommitId,
                CommitUrl = CommitUrl,
                CommitMessage = CommitMessage,
                CommitDate = CommitDate,
                CommitAuthor = CommitAuthor,
                CommitAuthorEmail = CommitAuthorEmail
            };
    }
}