using Newtonsoft.Json.Linq;

namespace ProWorks.AppVeyor.SlackProxy
{
    public class AppVeyorNotification
    {
        public string EventName { get; set; }
        public JToken EventData { get; set; }
    }
}
