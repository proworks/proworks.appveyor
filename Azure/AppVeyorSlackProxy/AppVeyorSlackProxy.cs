using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Net.Mail;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace ProWorks.AppVeyor.SlackProxy
{
    public static class AppVeyorSlackProxy
    {
        [FunctionName("AppVeyorSlackProxy")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            try
            {
                var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                var message = JsonConvert.DeserializeObject<AppVeyorNotification>(requestBody);

                log.LogInformation($"Handling {message?.EventName} message");
                switch (message.EventName)
                {
                    case "build_success":
                        await HandleBuildSuccess(message.EventData.ToObject<BuildData>(), log);
                        break;
                    case "build_failure":
                        await HandleBuildFailure(message.EventData.ToObject<BuildData>(), log);
                        break;
                    case "deployment_success":
                        await HandleDeploymentSuccess(message.EventData.ToObject<DeploymentData>(), log);
                        break;
                    case "deployment_failure":
                        await HandleDeploymentFailure(message.EventData.ToObject<DeploymentData>(), log);
                        break;
                    default:
                        return new BadRequestObjectResult("Unknown event name " + message.EventName);
                }

                return new OkResult();
            }
            catch (Exception ex)
            {
                log.LogError(ex, "UNHANDLED EXCEPTION");
                return new BadRequestResult();
            }
        }

        private static async Task HandleBuildSuccess(BuildData build, ILogger log) => await SendSlackIncomingWebHook(build.ToMessage(), log);

        private static async Task HandleBuildFailure(BuildData build, ILogger log)
        {
            await SendSlackIncomingWebHook(build.ToMessage(), log);
            await DmCommitter(build.ToMessage(), log);
        }

        private static async Task HandleDeploymentSuccess(DeploymentData deploy, ILogger log) => await SendSlackIncomingWebHook(deploy.ToMessage(), log);

        private static async Task HandleDeploymentFailure(DeploymentData deploy, ILogger log)
        {
            await SendSlackIncomingWebHook(deploy.ToMessage(), log);
            await DmCommitter(deploy.ToMessage(), log);
        }

        private static async Task SendSlackMessage(string url, string token, string channel, WebHookMessage message, ILogger log)
        {
            log.LogInformation("Sending Slack message '{0}' to channel '{1}' using {2}", message?.Name, channel, url);
            if (string.IsNullOrWhiteSpace(message?.Name) || string.IsNullOrWhiteSpace(url)) return;

            var emoji = message.Success ? ":heavy_check_mark:" : (message.Name.StartsWith(" of ") ? ":fire:" : ":x:");
            var status = message.Success ? "succeeded" : "failed";
            var msg = $"{emoji} <{message.Url}|{message.Type} {message.Name} {status}>";
            if (!string.IsNullOrWhiteSpace(message.CommitId))
            {
                msg += $"\r\nCommit <{message.CommitUrl}|{message.CommitId}> by {message.CommitAuthor} on {message.CommitDate}: _ {message.CommitMessage} _";
            }

            using (var client = new HttpClient())
            {
                var body = JsonConvert.SerializeObject(new
                {
                    username = "AppVeyor CI",
                    icon_url = "https://ci.appveyor.com/assets/images/appveyor-blue-144.png",
                    text = msg,
                    channel = channel
                });

                if (token != null)
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }

                var result = await client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"));
                var response = await result.Content.ReadAsStringAsync();
                log.LogInformation("Slack message sent with a result of {0} and a response of {1}", result.StatusCode.ToString(), response);
            }
        }

        private static async Task SendSlackIncomingWebHook(WebHookMessage message, ILogger log)
        {
            var url = Environment.GetEnvironmentVariable("SlackIncomingWebHookUrl");
            await SendSlackMessage(url, null, null, message, log);
        }

        private static async Task DmCommitter(WebHookMessage message, ILogger log)
        {
            var token = Environment.GetEnvironmentVariable("SlackApiToken");
            if (string.IsNullOrWhiteSpace(token))
            {
                await SendEmail(message, log);
                return;
            }

            var mapData = Environment.GetEnvironmentVariable("SlackUserMap");
            var map = string.IsNullOrWhiteSpace(mapData) || mapData[0] != '{' ? null : JsonConvert.DeserializeObject<Dictionary<string, string>>(mapData);
            map = map == null ? null : new Dictionary<string, string>(map, StringComparer.InvariantCultureIgnoreCase);
            
            if (map == null || !map.TryGetValue(message.CommitAuthorEmail, out var channel))
            {
                channel = await LookupSlackChannel(token, message.CommitAuthorEmail, log);

                if (string.IsNullOrWhiteSpace(channel))
                {
                    await SendEmail(message, log);
                    return;
                }
            }

            await SendSlackMessage("https://slack.com/api/chat.postMessage", token, channel, message, log);
        }

        private static async Task<string> LookupSlackChannel(string token, string email, ILogger log)
        {
            log.LogInformation($"Looking up channel for user {email}");

            using (var client = new HttpClient())
            {
                var result = await client.GetStringAsync($"https://slack.com/api/users.lookupByEmail?token={Uri.EscapeDataString(token)}&email={Uri.EscapeDataString(email)}");
                var json = JObject.Parse(result);
                var id = json?["user"]?["id"]?.ToString();
                if (string.IsNullOrWhiteSpace(id) || json["ok"]?.ToString().ToLowerInvariant() != "true") {
                    log.LogWarning($"Could not find user {email} - {json?["error"]?.ToString()}");
                    return null;
                }

                log.LogInformation($"Found user {id} for email {email}");
                result = await client.GetStringAsync($"https://slack.com/api/conversations.list?token={Uri.EscapeDataString(token)}&types=im&limit=1000");
                json = JObject.Parse(result);
                var channel = json?["channels"]?.FirstOrDefault(c => c?["user"]?.ToString() == id)?["id"]?.ToString();
                if (string.IsNullOrWhiteSpace(channel)) log.LogWarning($"Could not find channel for user {email} ({id})");
                else log.LogInformation($"Found channel {channel} for user {id} with email {email}");

                return channel;
            }
        }

        private static async Task SendEmail(WebHookMessage message, ILogger log)
        {
            var smtpHost = Environment.GetEnvironmentVariable("SmtpHost");
            var smtpPort = int.TryParse(Environment.GetEnvironmentVariable("SmtpPort"), out var p) && p > 0 && p < 65536 ? p : 25;

            log.LogInformation("Sending email message '{0}' to '{1}' using {2}", message?.Name, message?.CommitAuthorEmail, smtpHost);
            if (string.IsNullOrWhiteSpace(smtpHost) || string.IsNullOrWhiteSpace(message?.Name) || string.IsNullOrWhiteSpace(message.CommitAuthorEmail)) return;

            var msg = new MailMessage("noreply-appveyor-notification@proworks.com", message.CommitAuthorEmail);
            var status = message.Success ? "succeeded" : "FAILED";
            var body = $"<p><a href='{message.Url}'>{message.Type} {message.Name} {status}</a></p>";

            if (!string.IsNullOrWhiteSpace(message.CommitId))
            {
                body += $"<p>Commit <a href='{message.CommitUrl}'>{message.CommitId}</a> by {message.CommitAuthor} on {message.CommitDate}: {message.CommitMessage}</p>";
            }

            msg.Subject = $"{message.Type} {message.Name} {status}";
            msg.Body = body;
            msg.IsBodyHtml = true;

            using (var client = new SmtpClient(smtpHost, smtpPort))
            {
                await client.SendMailAsync(msg);
                log.LogInformation("Email message sent");
            }
        }
    }
}
