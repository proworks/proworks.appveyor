using Newtonsoft.Json.Linq;

namespace ProWorks.AppVeyor.SlackProxy
{
    public class Runnable
    {
        public bool Passed { get; set; }
        public bool Failed { get; set; }
        public string Status { get; set; }
        public string Started { get; set; }
        public string Finished { get; set; }
        public string Duration { get; set; }
        public JArray Messages { get; set; }
    }
}