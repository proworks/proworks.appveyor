# ProWorks.AppVeyor

This project provides scripts to be used by ProWorks' builds and deploys using the AppVeyor system.  The idea is that you can include an init line to grab the contents of this package, and then be able to reference and use the scripts throughout the process.

This documentation may be outdated, ask someone about the latest process.

#### Outline for new projects

This process fits most projects, some projects require more technical setup

1. Create a new yml for the project
2. Create a new AppVeyor project
3. Set up the website in IIS
4. Create deployment environment(s)
5. Set up an AppVeyor agent for each destination
6. Modify the `.csproj`
7. Test and adjust as needed

## Adding a project yml

The project will need a `projectname.yml` file. These are stored in a secure repo called `proworks.appveyor.yaml`, check out the readme in that repo.

## Create a New Project on AppVeyor

1. Navigate to [https://ci.appveyor.com/](https://ci.appveyor.com/) and click **NEW PROJECT**
2. Select the proper repository. The repository needs to be available to the **ProworksDevTeam** account on GitHub or the **bcdesign** account on BitBucket
3. On the project settings page (found by clicking on the project name and then the settings tab), look for **Custom configuration .yml file name** and paste in a link to the yml file. This could be hosted on our private mirror (take a look at other projects) or directly at https://bitbucket.org/proworks/proworks.appveyor/raw/master/ProjectYAMLs/ProjectName.yml
4. Set **Default branch** to dev, it will change the behavior of the default build action.
5. If the yml file is set up correctly, the project should build successfully. A build can be started from the **Current build** tab.

## Set Up an IIS Website
Create a new IIS website, including
 * A root folder (May contain Media and App_Data)
 * Bindings (https bindings are set up automatically for sites on ProWorks Dev)
 * App Pool (should be made automatically)

A script located at `$env:ProgramData + "\ProWorks\AppVeyor\Global-Deploy.ps1"` on IISDev should automatically run win-acme and set up https bindings and certificates. 
DNS will work for IISDev via the wildcard `*.proworks.xyz` DNS record

## Create an Environment for Deployments

1. Navigate to [https://ci.appveyor.com/environments/](https://ci.appveyor.com/environments/) and click **NEW ENVIRONMENT**
2. Select **AppVeyor Agent**
3. Follow the naming scheme of other Environments: Project, Server Name/Service, and Environment
* `Planar.WSPROD30.Prod` follows `ProjectName.ServerName.Environment`
* `USDA.ERS.Azure.Dev.Bridge` follows `ProjectName.SubProject.ServiceName.Environment.Component`
4. Click **Add Environment**, then go to **Settings**
5. For ProWorks Dev Environments, the **Environment access key** can be copied from an existing ProWorks Dev Environment (ProjectName.PW.Dev). Be sure to set the Environment access key to the correct value. If you will be setting up a new AppVeyor Agent, then save the randomly generated Environment access key for later.
7. Add an **Environment variable** called `AllowedBranches`. The value should probably be `dev`, `master` or `release`. See **Branch name verification before deploy** documentation below
8. Under **Provider settings**, Click **Add setting** until you have enough blank fields for the table below. Copy and paste all the values.

|Name|Value|Purpose|
|--|--|--|
|websitezip.deploy_website|`true`|Tells AppVeyor Agent to deploy an IIS Website|
|websitezip.site_name|IIS website name goes here|Allows agent to find website in IIS|
|websitezip.path|Like `D:\wwwroot\ProjectName\Dev`|Disk path of website|
|websitezip.skip_dirs|`^[^\\]*\\media;^[^\\]*\\app_data`|AppVeyor won't delete files in these folders (RegEx)|
|websitezip.write_access|`true`|Add AppPool to authorized users|
|websitezip.remove_files|`true`|Delete files on destination that are not in deploy zip|
|websitezip.skip_files|`deploy.ps1`|Skip deletion of these files|
|websitezip.app_offline|true|Creates app_offline.htm to prevent write errors|

## Set Up an AppVeyor Agent

For Windows Server 2008, an older version of the Agent must be used. AppVeyor Deployment Agent v3.52.0 is known to work. For newer versions of Windows, the latest version should work fine. Downloads are found at [https://www.appveyor.com/docs/deployment/agent/](https://www.appveyor.com/docs/deployment/agent/)

1. Run the setup or powershell command
2. Use the randomly generated Environment access key obtained by creating a new Environment on the AppVeyor website.

## Include Files in Deploy Package

1. Open the `.csproj` file for the website project (the one with App_Data and umbraco folders, and css/js)
2. Alongside other `<Target>` elements, create a new `<Target>` element with the appropriate directories and files that need to be deployed in a release package

```xml
  <Target Name="AdditionalFilesForPackage" AfterTargets="CopyAllFilesToSingleFolderForPackage;CopyAllFilesToSingleFolderForMsdeploy">
    <ItemGroup>
      <Files Include="App_Browsers\*" />
      <Files Include="App_Data\packages\**" />
      <Files Include="App_Plugins\**" />
      <Files Include="config\**" />
      <Files Include="images\**" />
      <Files Include="scripts\**" />
      <Files Include="css\**" />
      <Files Include="dist\**" />
      <Files Include="uSync\**" />
      <Files Include="robots.txt" />
      <Licenses Include="bin\*.lic" />
    </ItemGroup>
    <Copy SourceFiles="@(Files)" DestinationFolder="$(_PackageTempDir)\%(RelativeDir)" />
    <Copy SourceFiles="@(Licenses)" DestinationFolder="$(_PackageTempDir)\bin" />
  </Target>
```

## Versioning

When a deploy is approved, create a release branch. The name is based on the Umbraco version and a made up number like `4.2`. Just check the yml for the version, then create a matching release branch from master.

1. Login into to BitBucket and go to the ProWorks.AppVeyor.YAML repository. Open the prjects yml file.
2. Example of version in the yml file: `version: '7153.4.2.{build}'`. The `7153` represents the Umbraco version number. If the Umbraco versions changes then update the yml.
3. All three numbers must match the appveyor.yml file. For Umbraco `7.15.3` release version `4.2`, the branch name should be `release/7153/4.2`. The yml should read `version: '7153.4.2.{build}'`. 

## Automatic Version Incrementing

If the YML file is stored in the ProWorks.AppVeyor.YAML Bitbucket repository, OnSuccess script will increment the version in `ProjectName.yml` file after a successful release branch build or tagged build.

A yml version of `7153.1.22` will increment to `7153.1.23`. Only the last number will change, other version changes will require manually editing the yml.

Tags are for packages and release branches are for Umbraco websites. When you tag a release build, use the version as the tag name like  `v1.4.2`.

#### If the YML file is stored outside of ProWorks.AppVeyor.YAML

You may need to do a few things to set up automatic version incrementing if the Proworks.AppVeyor Bitbucket repository is not being used.

1. Create a Git credential URL. This varies by service, but generally takes the form of `https://{user}:{pass}@{host}`. There is a GitCredential environment variable defined in appveyor.yml. See [https://www.appveyor.com/docs/how-to/git-push/](https://www.appveyor.com/docs/how-to/git-push/) for details.
2. Encrypt the URL: Log into the AppVeyor interface, and go to the Account section. Click on Encrypt YAML on the left. Paste in the full URL and copy the encrypted value
3. Put the encrypted URL as an environment variable on the project, with a variable name of `GitCredential`
4. Make a copy of `OnSuccess.ps1` and modify as needed, it should point to the proper repository. You could copy the powershell script directly into the yml or add it to the project's own repository and reference it.

## Automated Powershell Tests

These are basic scripted tests to catch errors before deployment. These tests should offer helpful details in the build log. Example build showing output for version, App_Data and csproj files tests: [https://ci.appveyor.com/project/Proworks/appveyor-test-teclabsinc/builds/28331059](https://ci.appveyor.com/project/Proworks/appveyor-test-teclabsinc/builds/28331059)

#### Branch name verification before deploy

An environment variable called `AllowedBranches` should be set in the deploy Environment. It should contain a single branch such as `release` or list of allowed branched such as `dev master`. A deploy will only succeed if `AllowedBranches` is empty or if the build branch matches `AllowedBranches`. Only the first part if the branch name will be matched, for example `release` in `release/1234/1.2`

#### Version number verification

For easy rollbacks and to prevent deployment mistakes, version strings are verified.
The code can be found in `SetVersion.ps1`. The branch name and the version in the yml must match. Also, the Umbraco version must match (for Umbraco installed via NuGet).

|Version Identifier|Value|
|--|--|
|Branch name | `release/7153/4.2`|
|Version in `.yml` file | `version: '7153.4.2.{build}'`|
|Umbraco version in `packages.config` | `7.15.3`|

#### Files in App_Data

If files are found in `App_Data` the build will fail (e.g. UmbracoForms json). Exceptions to the rule are `access.config`, `createdPackages.config` and `installedPackages.config`. Additional exceptions can be added by modifying the `Tests.ps1` script.
This rule can be bypassed completely if the project needs to deploy files to `App_Data`. A reminder of how to do this can be found in the failed build log. Just add an environment variable to the yml file:

```yaml
environment:
  AllowAppData: true
```

#### Files listed in csproj that don't exist

The list of included files in all `.csproj` files are verified with the powershell `Test-Path` command. One cause of this error is when a file is deleted in Windows Explorer rather than Visual Studio. If a file is included in Visual Studio but it does not actually exist, the build will fail due to the way MSDeploy is set up. This helps ensure that all required files are there.

## Modifying the Scripts

The yml files trigger a download of this repo and run the PowerShell scripts. You can modify the .ps1 files in the scripts directory. Pushing to the master branch will update all builds and deployments using the package. Use caution.

A list of AppVeyor environment variables can be found at https://www.appveyor.com/docs/environment-variables/.  Other AppVeyor documentation and reference information can be found at the same place.

For a good primer on PowerShell, see https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/.  Other PowerShell documentation can also be found there, as well as many other sources on line.

Powershell Punctuation like `#` `$()` `${}` `@( )` `::` `&` [The Complete Guide to PowerShell Punctuation](https://www.red-gate.com/simple-talk/sysadmin/powershell/the-complete-guide-to-powershell-punctuation/)