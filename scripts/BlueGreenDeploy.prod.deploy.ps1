Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Starting Blue/Green switch from beta to production')

$BetaSiteName = [string]$env:BlueGreen_BetaSiteName
$ProdSiteName = [string]$env:BlueGreen_ProdSiteName
$DbConnection = [string]$env:BlueGreen_DbConnection

if ($null -eq $BetaSiteName -or $BetaSiteName -eq '') { throw 'The BlueGreen_BetaSiteName environment variable is not set'; Exit 1 }
if ($null -eq $ProdSiteName -or $ProdSiteName -eq '') { throw 'The BlueGreen_ProdSiteName environment variable is not set'; Exit 1 }
if ($null -eq $DbConnection -or $DbConnection -eq '') { throw 'The BlueGreen_DbConnection environment variable is not set'; Exit 1 }

Import-Module WebAdministration

if (!(Test-Path "IIS:\Sites\$BetaSiteName")) { throw "Could not find the IIS beta site $BetaSiteName"; Exit 1 }
if (!(Test-Path "IIS:\Sites\$ProdSiteName")) { throw "Could not find the IIS prod site $ProdSiteName"; Exit 1 }

$BetaAppPool = [string]$env:BlueGreen_BetaAppPool
if ($null -eq $BetaAppPool -or $BetaAppPool.Trim().Length -eq 0) {
    $BetaAppPool = Get-ItemProperty -Path "IIS:\Sites\$BetaSiteName" -Name 'applicationPool'
}
if ($null -eq $BetaAppPool -or $BetaAppPool -eq '') { throw 'The BlueGreen_BetaAppPool environment variable is not set'; Exit 1 }

$ProdAppPool = [string]$env:BlueGreen_ProdAppPool
if ($null -eq $ProdAppPool -or $ProdAppPool.Trim().Length -eq 0) {
    $ProdAppPool = Get-ItemProperty -Path "IIS:\Sites\$ProdSiteName" -Name 'applicationPool'
}
if ($null -eq $ProdAppPool -or $ProdAppPool -eq '') { throw 'The BlueGreen_ProdAppPool environment variable is not set'; Exit 1 }

if (!(Test-Path "IIS:\AppPools\$BetaAppPool")) { throw "Could not find the IIS beta app pool $BetaAppPool"; Exit 1 }
if (!(Test-Path "IIS:\AppPools\$ProdAppPool")) { throw "Could not find the IIS prod app pool $ProdAppPool"; Exit 1 }

$BetaSitePath = Get-ItemProperty -Path "IIS:\Sites\$BetaSiteName" -Name 'physicalPath'
$ProdSitePath = Get-ItemProperty -Path "IIS:\Sites\$ProdSiteName" -Name 'physicalPath'

if (!(Test-Path $BetaSitePath\Web.config)) { throw "Could not find the web.config file in the path $BetaSitePath for the IIS beta site $BetaSiteName"; Exit 1 }
if (!(Test-Path $ProdSitePath\Web.config)) { throw "Could not find the web.config file in the path $ProdSitePath for the IIS prod site $ProdSiteName"; Exit 1 }

$xml = [xml](Get-Content $BetaSitePath\Web.config)
$dbKey = $xml.SelectSingleNode("/configuration/connectionStrings/add[@name='umbracoDbDSN']")
$dbConn = [string]$dbKey.GetAttribute("connectionString")
$dbConnParts = $dbConn -split ';'
$databasePart = ([string]($dbConnParts | Where-Object { $_ -imatch '^\s*(DataBase|Initial Catalog)\s*=' })).Trim()
$betaDbName = [string](($databasePart -split '=')[1])

$xml = [xml](Get-Content $ProdSitePath\Web.config)
$dbKey = $xml.SelectSingleNode("/configuration/connectionStrings/add[@name='umbracoDbDSN']")
$dbConn = [string]$dbKey.GetAttribute("connectionString")
$dbConnParts = $dbConn -split ';'
$databasePart = ([string]($dbConnParts | Where-Object { $_ -imatch '^\s*(DataBase|Initial Catalog)\s*=' })).Trim()
$prodDbName = [string](($databasePart -split '=')[1])
$oldProdDbName = $betaDbName + '_OLD'

if ($betaDbName -eq $prodDbName) { throw "The production database name and the beta database name are both $prodDbName"; Exit 1 }

Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Connecting to the database')
$Conn = New-Object System.Data.SqlClient.SqlConnection $DbConnection
$Conn.Open()
if ($Conn.State -ne 'Open') { $Conn.Dispose(); throw "Could not open a connection to the database"; Exit 1 }
$Cmd = New-Object System.Data.SqlClient.SqlCommand "SELECT DB_ID('$betaDbName')",$Conn
$ExistBetaDb = $Cmd.ExecuteScalar() -ne [System.DBNull]::Value
$Cmd.Dispose()

$Cmd = New-Object System.Data.SqlClient.SqlCommand "SELECT DB_ID('$prodDbName')",$Conn
$ExistProdDb = $Cmd.ExecuteScalar() -ne [System.DBNull]::Value
$Cmd.Dispose()

$ExistOldProdDb = $true
$NameSuffix = 0
while ($ExistOldProdDb) {
    $NameSuffix += 1
    $TestDb = $betaDbName + '_OLD_' + $NameSuffix
    $Cmd = New-Object System.Data.SqlClient.SqlCommand "SELECT DB_ID('$TestDb')",$Conn
    $ExistOldProdDb = $Cmd.ExecuteScalar() -ne [System.DBNull]::Value
    $Cmd.Dispose()
}
$oldProdDbName = $TestDb

if (!$ExistBetaDb) { $Conn.Dispose(); throw "Could not find the beta database $betaDbName"; Exit 1 }
if (!$ExistProdDb) { $Conn.Dispose(); throw "Could not find the prod database $prodDbName"; Exit 1 }

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Stopping the beta website')
Stop-Website -Name $BetaSiteName
Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Stopping the beta app pool')
Stop-WebAppPool -Name $BetaAppPool

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Stopping the prod website')
Stop-Website -Name $ProdSiteName

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Switching the physical path of the prod website from $ProdSitePath to $BetaSitePath")
Set-ItemProperty -Path "IIS:\Sites\$ProdSiteName" -Name 'physicalPath' -Value "$BetaSitePath"

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Renaming the production database from $prodDbName to $oldProdDbName")
$cmd = New-Object System.Data.SqlClient.SqlCommand "ALTER DATABASE [$prodDbName] SET SINGLE_USER WITH ROLLBACK IMMEDIATE; ALTER DATABASE [$prodDbName] MODIFY NAME = [$oldProdDbName]; ALTER DATABASE [$oldProdDbName] SET MULTI_USER",$Conn
$cmd.ExecuteNonQuery()
$cmd.Dispose()

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Renaming the beta database from $betaDbName to $prodDbName")
$cmd = New-Object System.Data.SqlClient.SqlCommand "ALTER DATABASE [$betaDbName] SET SINGLE_USER WITH ROLLBACK IMMEDIATE; ALTER DATABASE [$betaDbName] MODIFY NAME = [$prodDbName]; ALTER DATABASE [$prodDbName] SET MULTI_USER",$Conn
$cmd.ExecuteNonQuery()
$cmd.Dispose()

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Updating the database connection string for the new prod website to point to the prod database $prodDbName")
$xml = [xml](Get-Content $BetaSitePath\Web.config)
$dbKey = $xml.SelectSingleNode("/configuration/connectionStrings/add[@name='umbracoDbDSN']")
$dbConn = [string]$dbKey.GetAttribute("connectionString")
$dbConnParts = $dbConn -split ';'
$cnt = 0
while ($cnt -lt $dbConnParts.Length) {
    if ($dbConnParts[$cnt] -imatch '^\s*(DataBase|Initial Catalog)\s*=') { $dbConnParts[$cnt] = ($dbConnParts[$cnt] -split '=')[0] + '=' + $prodDbName }
    $cnt = $cnt + 1
}
$dbKey.SetAttribute('connectionString', ($dbConnParts -join ';'))
$xml.Save("$BetaSitePath\web.config")

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Updating the database connection string for the old prod website to point to the old prod database $oldProdDbName")
$xml = [xml](Get-Content $ProdSitePath\Web.config)
$dbKey = $xml.SelectSingleNode("/configuration/connectionStrings/add[@name='umbracoDbDSN']")
$dbConn = [string]$dbKey.GetAttribute("connectionString")
$dbConnParts = $dbConn -split ';'
$cnt = 0
while ($cnt -lt $dbConnParts.Length) {
    if ($dbConnParts[$cnt] -imatch '^\s*(DataBase|Initial Catalog)\s*=') { $dbConnParts[$cnt] = ($dbConnParts[$cnt] -split '=')[0] + '=' + $oldProdDbName }
    $cnt = $cnt + 1
}
$dbKey.SetAttribute('connectionString', ($dbConnParts -join ';'))
$xml.Save("$ProdSitePath\web.config")

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Recycling the prod app pool')
Restart-WebAppPool -Name $ProdAppPool

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Starting the prod website')
Start-Website -Name $ProdSiteName

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Finished the Blue/Green switch from beta to production')

$Conn.Dispose()

$bindings = (Get-WebBinding -Name $ProdSiteName | Where-Object { $_.protocol -eq 'http' })
$binding = $null
if ($bindings.Length -eq 0) {
    $bindings = (Get-WebBinding -Name $ProdSiteName | Where-Object { $_.protocol -eq 'https' })
}
if ($bindings.Length -eq 1) {
    $binding = $bindings
} elseif ($bindings.Length -gt 1) {
    $binding = $bindings[0]
}

if ($null -ne $binding) {
    try
    {
        $bindingParts = $binding.bindingInformation -split ':'
        $bindingPort = $bindingParts[1]
        $bindingHost = $bindingParts[2]
        if ($bindingHost -eq '') {
            $bindingHost = 'localhost'
        }
        if ($bindingParts[0] -eq '*') {
            $bindingServer = $bindingHost
        } else {
            $bindingServer = $bindingParts[0]
        }

        $scheme = $binding.protocol
        $prodUrl = "${scheme}://${bindingServer}:${bindingPort}/umbraco/"
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Attempting to invoke the prod site at $prodUrl with a Host header of $bindingHost")

        $resp = Invoke-WebRequest -Uri "$prodUrl" -Headers @{"Host"=$bindingHost}
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -     Got a status code of " + $resp.StatusCode)
    }
    catch {
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -     WARNING: Could not successfully invoke the prod site.  You will need to manually access the site.")
    }
} else {
    Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - WARNING: Could not find a suitable binding to attemp to invoke the prod site at.  You will need to manually access the site.")
}
