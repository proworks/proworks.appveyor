# You can place any common functions you want to use in multiple other scripts here.  They will be sourced in by the Init.ps1 script, and be available to all other functions.

$callingScript = (Get-PSCallStack)[1].Command
if ($null -ne $env:APPVEYOR_RDP_PASSWORD -and $env:APPVEYOR_RDP_PASSWORD.Trim().Length -gt 0)
{
    $breakScript = ($env:APPVEYOR_RDP_BREAKON + '.ps1')
    if ("$breakScript" -eq "$callingScript" -or $env:APPVEYOR_RDP_BREAKON -eq "$callingScript")
    {
        $blockRdp = $true
        iex ((new-object net.webclient).DownloadString('https://raw.githubusercontent.com/appveyor/ci/master/scripts/enable-rdp.ps1'))
    }
    # else
    # {
    #     Write-Host ("Not blocking because " + $env:APPVEYOR_RDP_BREAKON + ".ps1 does not match $callingScript")
    # }
}

function Get-DeployRoot()
{
    param(
        [string]$ZipFile
    )

    [Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem') | Out-Null
    $zip = [System.IO.Compression.ZipFile]::Open($ZipFile, "Read")
    $entry = $zip.GetEntry("archive.xml");
    $rdr = New-Object 'System.IO.StreamReader' $entry.Open()
    $line = $rdr.ReadLine()
    $entryPrefix = $null
    while ($null -ne $line -and $null -eq $entryPrefix)
    {
        if ($line -match '<iisApp path="')
        {
            $entryPrefix = ($line -replace '.*<iisApp path="([^"]+)".*','$1') -replace 'C:','Content\C_C'
        }
        $line = $rdr.ReadLine()
    }
    $rdr.Dispose()
    $zip.Dispose()

    $entryPrefix
}

function Add-DeployRootFile()
{
    param(
        [string]$ZipFile,
        [string]$EntryPrefix,
        [string[]]$File,
        [string[]]$Name
    )

    Write-Output "Adding files to $ZipFile"
    [Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem') | Out-Null
    $zip = [System.IO.Compression.ZipFile]::Open($ZipFile, "Update")

    $idx = 0
    while ($idx -lt $File.Length)
    {
        $flPath = $File[$idx]
        $eName = $Name[$idx]
        $entryName = ($EntryPrefix + '\' + $eName)
        Write-Output "    Adding $eName"
        [System.IO.Compression.ZipFileExtensions]::CreateEntryFromFile($zip, $flPath, $entryName, "Optimal") | Out-Null

        $idx = $idx + 1
    }

    $zip.Dispose()
}

function Deploy-Delayed()
{
    param(
        [string[]]$EnvironmentNames,
        [switch]$WaitForCompletion,
        [int]$DelaySeconds = 20,
        [string]$ApiKey = $env:AV_API_KEY,
        [int]$MaxRetryCount = 180
    )

    if ($null -eq $ApiKey -or $ApiKey.Trim().Length -eq 0) {
        throw "No valid API key present"
        return
    }

    $headers = @{
        "Authorization" = "Bearer $ApiKey"
        "Content-Type" = "application/json"
    }

    Write-Host "Deploying using the following environment variables:"
    Write-Host "    Account Name:  $env:APPVEYOR_ACCOUNT_NAME"
    Write-Host "    Project Slug:  $env:APPVEYOR_PROJECT_SLUG"
    Write-Host "    Build Version: $env:APPVEYOR_BUILD_VERSION"
    Write-Host "    Build Job ID:  $env:APPVEYOR_JOB_ID"

    [bool]$FirstEnv = $true
    ForEach($eName in $EnvironmentNames) {
        if ($FirstEnv) {
            $FirstEnv = $false
        } elseif (!$WaitForCompletion) {
            Write-Host "    Sleeping $DelaySeconds seconds before starting next environment..."
            Start-Sleep -Seconds $DelaySeconds
        }

        $body = @{
            environmentName=$eName
            accountName=$env:APPVEYOR_ACCOUNT_NAME
            projectSlug=$env:APPVEYOR_PROJECT_SLUG
            buildVersion=$env:APPVEYOR_BUILD_VERSION
            buildJobId=$env:APPVEYOR_JOB_ID
        }
        $body = $body | ConvertTo-Json
        Write-Host "Starting deploy to environment $eName"
        $deploymentId = (Invoke-RestMethod -Uri 'https://ci.appveyor.com/api/deployments' -Headers $headers -Body $body -Method POST).deploymentId

        [bool]$deployDone = !$WaitForCompletion
        [int]$retryCount = 0
        while (!$deployDone) {
            if ($retryCount -gt $MaxRetryCount) {
                Write-Host "    Reached maximum number of retries of $MaxRetryCount, skipping waiting for environment completion"
                $deployDone = $true
            } else {
                $deployStatus = (Invoke-RestMethod -Uri "https://ci.appveyor.com/api/deployments/$deploymentId" -Headers $headers  -Method Get).deployment.status
                $deployDone = $deployStatus -eq 'success'

                if ($deployDone) {
                    Write-Host "    Current deployment status: $deployStatus"
                } else {
                    Write-Host "    Current deployment status: $deployStatus, sleeping $DelaySeconds seconds before next check..."
                    Start-Sleep -Seconds $DelaySeconds
                    $retryCount++
                }
            }
        }
    }
}

function Find-MatchingFiles()
{
    param(
        [string]$FileName,
        [string]$RelativePaths = $null,
        [string]$Depth = $null
    )

    $PathNames = $null
    $MaxDepth = 3

    if ($null -ne $RelativePaths -and $RelativePaths.Trim().Length -gt 0) {
        $PathNames = $RelativePaths.Split(',')
        $MaxDepth = 0
    }

    $DepthVal = 0
    if ($null -ne $Depth -and [int]::TryParse($Depth, [ref]$DepthVal)) { $MaxDepth = $DepthVal }

    if ($null -ne $PathNames -and $PathNames.Count -gt 0) {
        ForEach ($PathName in $PathNames) {
            Get-ChildItem "$env:APPVEYOR_BUILD_FOLDER/$PathName" -Filter $FileName -Depth $MaxDepth
        }
    } else {
        Get-ChildItem "$env:APPVEYOR_BUILD_FOLDER" -Filter $FileName -Depth $MaxDepth
    }
}