. $PSScriptRoot/CommonFunctions.ps1

if ($env:SetDeployDate -eq 'true' -or $env:RollingBlueGreen -eq 'true')
{
    $deploy_date=Get-Date -Format 'yyyyMMdd_HHmmss'
    Set-AppveyorBuildVariable -Name deploy_date -Value $deploy_date
    if ($env:BlueGreenDeployment -eq 'true' -or $env:RollingBlueGreen -eq 'true')
    {
        Set-AppveyorBuildVariable -Name BlueGreen_DeployDate -Value $deploy_date

        # Set the umbracoDbDSN connection string to an invalid value so that the beta site doesn't try to start up before we've done a backup/restore and are ready for it
        Set-AppveyorBuildVariable -Name 'umbracoDbDSN-Web.config Connection String' -Value "${deploy_date}"
    }
}
