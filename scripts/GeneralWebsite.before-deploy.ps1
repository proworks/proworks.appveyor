$deployRoot = '@deployRoot@'
$buildBranch = [regex]::match($env:APPVEYOR_REPO_BRANCH, '(^[^/]*)').Groups[1].Value
$allowedBranches = [string]$env:AllowedBranches

# $buildBranch is the name of the branch that was built (everything before the first slash)
# If $buildBranch is not listed in $allowedBranches, fail the deploy
if ($null -ne $allowedBranches -and $allowedBranches.Trim().Length -gt 0) {
    if ($allowedBranches.contains($buildBranch) -eq $false) {
        Write-Warning "This build is from the '$buildBranch' branch. The deploy Environment has an AppVeyor environment variable called AllowedBranches which is set to: '$allowedBranches'."
        Write-Warning "You can add '$buildBranch' to AllowedBranches if you want to deploy to this Environment."
        throw "AllowedBranches environment variable"
    }
    Write-Output "This build is from the '$buildBranch' branch. The deploy Environment has an AppVeyor environment variable called AllowedBranches which is set to: '$allowedBranches'."
    Remove-Item env:/AllowedBranches
}

Import-Module WebAdministration

$SitePath = $env:APPLICATION_PATH
# If we specify the websitezip.path environment setting, then the $env:APPLICATION_PATH is set to that, otherwise it is set to folder under C:\appveyor\projects
if ($SitePath -imatch '^C:\\appveyor\\projects\\') {
    $SitePath = (Get-Website -Name $env:APPLICATION_SITE_NAME).PhysicalPath
}

if (Test-Path "$SitePath\deploy.ps1") {
    Write-Output "Deleting existing deploy.ps1"
    Remove-Item "$SitePath\deploy.ps1" -Force -Confirm:$false
}

if (!(Test-Path "$SitePath")) {
    Write-Output "Creating output directory"
    New-Item -ItemType Directory "$SitePath" -Force -Confirm:$false
}

try {
    [Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem") | Out-Null
    Write-Output "Extracting deploy.ps1 from the artifact at $env:ARTIFACT_LOCALPATH"
    
    try {
        $zip = [System.IO.Compression.ZipFile]::OpenRead("$env:ARTIFACT_LOCALPATH");
    }
    catch {
        $zip = $null
    }
    
    if ($null -eq $zip) {
        Write-Output "Could not find or open the artifact"
        exit
    }
    
    try {
        $entry = $zip.GetEntry("${deployRoot}deploy.ps1");
    }
    catch {
        $entry = $null
    }
    
    if ($null -eq $entry) {
        Write-Output "Could not find ${deployRoot}deploy.ps1 in the artifact"
        exit
    }
    
    Write-Output "Extracting deploy.ps1 to $SitePath"
    [System.IO.Compression.ZipFileExtensions]::ExtractToFile($entry, "$SitePath\deploy.ps1")
}
catch {
    & 'C:\Program Files\7-Zip\7z.exe' e "$env:ARTIFACT_LOCALPATH" -o "$SitePath" -y "${deployRoot}deploy.ps1"
}

if (Test-Path "$SitePath\deploy.ps1") {
    Write-Output "Successfully extracted deploy.ps1"
}
else {
    Write-Output "Could not extract deploy.ps1"
}

$globalBeforeDeploy = ($env:ProgramData + "\ProWorks\AppVeyor\Global-Before-Deploy.ps1")
if (Test-Path $globalBeforeDeploy) {
    Write-Output "Executing $globalBeforeDeploy"
    . $globalBeforeDeploy
}
else {
    Write-Output "No global before-deploy file found at $globalBeforeDeploy"
}
