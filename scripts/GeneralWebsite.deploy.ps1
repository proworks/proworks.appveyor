$globalDeploy=($env:ProgramData + "\ProWorks\AppVeyor\Global-Deploy.ps1")

if (Test-Path $globalDeploy)
{
    Write-Output "Executing $globalDeploy"
    . $globalDeploy
}
else
{
    Write-Output "No global deploy file found at $globalDeploy"
}