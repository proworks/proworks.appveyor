if ($null -ne $env:APPLICATION_SITE_NAME -and $env:APPLICATION_SITE_NAME -ne "")
{
    $WebSite = Get-Website -Name $env:APPLICATION_SITE_NAME
    if ($null -ne $WebSite)
    {
        $AppPool = $WebSite.applicationPool

        if ($null -ne $AppPool -and $AppPool -ne "")
        {
            Import-Module WebAdministration
            $AppPoolState = (Get-WebAppPoolState -Name $AppPool).Value

            if ($AppPoolState -eq 'Started')
            {
                Stop-WebAppPool -Name $AppPool

                $iterations = 0
                While (((Get-WebAppPoolState -Name $AppPool).Value) -ne 'Stopped' -and $iterations -le 90)
                {
                    Start-Sleep 1
                    $iterations += 1
                }
            }

            $AppPoolState = (Get-WebAppPoolState -Name $AppPool).Value
            if ($AppPoolState -ne 'Stopped')
            {
                Write-Output "Could not stop the application pool $AppPool for site $env:APPLICATION_SITE_NAME, the final state was $AppPoolState"
            }
            else
            {
                Write-Output "Successfully stopped the application pool $AppPool for site $env:APPLICATION_SITE_NAME"
            }
        }
        else
        {
            Write-Output "Could not find an application pool for the website $env:APPLICATION_SITE_NAME"
        }
    }
    else
    {
        Write-Output "Could not find website $env:APPLICATION_SITE_NAME"
    }
}