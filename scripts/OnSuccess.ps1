. $PSScriptRoot/CommonFunctions.ps1

# GitCredential should be set to https://{user}:{app_password}@bitbucket.org, or whatever the URL is for the other service
# Use the AppVeyor, Account, Encrypt YAML feature to encrypt this value before saving it in the project YAML
$gitcred = [string]$env:GitCredential
$nextver = [string]$env:PROJECT_NEXT_VERSION
$YMLName = [string]$env:YMLName
$pullRequest = [string]$env:APPVEYOR_PULL_REQUEST_HEAD_REPO_BRANCH

Write-Output "Running OnSuccess.ps1"
if ($nextver.Trim().Length -gt 0 -and $gitcred.Trim().Length -gt 0 -and $YMLName.Length -gt 0 -and $pullRequest.Length -eq 0) {
        Write-Output "Updating version in yml to $nextver"
        $gituser = [string]$env:GIT_USER_NAME
        $gitemail = [string]$env:GIT_USER_EMAIL
        if ($null -eq $gituser -or $gituser.Trim().Length -eq 0) { $gituser = 'ProWorks AppVeyor' }
        if ($null -eq $gitemail -or $gitemail.Trim().Length -eq 0) { $gitemail = 'contact@proworks.com' }
        git config --global credential.helper store
        git config --global core.autocrlf "false"
        git config --global user.email "$gitemail"
        git config --global user.name "$gituser"

        Set-Content -Path "$HOME\.git-credentials" -Value "$gitcred`n"

        git clone --depth 1 --branch master --quiet https://bitbucket.org/proworks/proworks.appveyor.yaml.git $env:TMP\proworks.appveyor
        Set-Location $env:TMP\proworks.appveyor
        $ymlFile = (Get-ChildItem -Path $env:TMP\proworks.appveyor -Filter $YMLName -Recurse -Depth 3)[0].FullName

        $ptn = [System.Text.RegularExpressions.Regex]'^version: .*'
        git pull --quiet
        $ptn.Replace((Get-Content $ymlFile -Raw), "version: '" + $nextver + ".{build}'") | Set-Content -NoNewline $ymlFile
        git commit -a -m "Updating version in $YMLName to $nextver" '-v'
        git push --quiet

        $headers = @{
            "Authorization" = "Bearer $env:AppVeyorAPIKey"
            "Content-type" = "application/json"
        }
        $json = @{ nextBuildNumber = 1 } | ConvertTo-Json
        Invoke-RestMethod -Uri "https://ci.appveyor.com/api/projects/Proworks/$env:APPVEYOR_PROJECT_SLUG/settings/build-number" -Headers $headers -Method Put -Body $json
}
