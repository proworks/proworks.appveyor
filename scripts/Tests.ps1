# Place any test functions you want to use in here
function Check-AppData-Files()
{
    # Check for unwanted App_Data files
    foreach ($appDataPath in (Get-ChildItem -Path $env:APPVEYOR_BUILD_FOLDER -Recurse -Filter 'app_data' -Depth 3 -Attributes Directory | ForEach-Object FullName))
    {
        # Look at files in App_Data, except exclusions
        foreach ($file in (Get-ChildItem -Path $appDataPath -Recurse -Attributes !Directory | ForEach-Object FullName))
        {
            if ($file -notmatch 'app_data\\access.config$|app_data\\packages\\created\\createdPackages\.config$|app_data\\packages\\installed\\installedPackages\.config$')
            {
                Write-Warning "Found file in App_Data: $file"
                $foundAppDataFiles = $true
            }
        }
    }
    if ($foundAppDataFiles -and $null -eq $env:AllowAppData)
    {
        Write-Warning "`n`nTo bypass this error, add an environment variable named AllowAppData in the yml. For example: `nenvironment: `n  AllowAppData: true"
        $host.SetShouldExit(1)
        Exit
    }
}

function Check-Csproj-Included-Files()
{
    # Check for files included in csproj that don't exist
    foreach ($csprojfile in (Get-ChildItem -Path $env:APPVEYOR_BUILD_FOLDER -Filter '*.csproj' -Recurse -Depth 3))
    {
        $introwarning = $true
        Set-Location $csprojfile.DirectoryName;
        Write-Output "$(Write-Output $csprojfile.DirectoryName)\$(Write-Output $csprojfile.Name) - Checking for files included in csproj that don't exist"
        foreach ($line in Get-Content $csprojfile.Name)
        {
            if ($line -match '<Content.+Include="([^"]+)"')
            {
                $path = $Matches.1
                if ($path -match '[%&][234a][547B0m]') # Match only MSBuild special character escape strings
                {
                    $path = $path.Replace('%25','%').Replace('%24','$').Replace('%27',"'").Replace('%3B',';').Replace('%40','@').Replace('&amp;','&')
                }
                if (-not (Test-Path $path))
                {
                    if ($introwarning){
                        Write-Warning "Some files listed in $(Write-Output $csprojfile.Name) don't actually exist:"
                        $introwarning = $false
                    }
                    Write-Warning "$(Write-Output $csprojfile.Name):$(Write-Output $path)`n"
                    Add-AppveyorMessage -Message "A file included in a csproj does not exist" -Category Warning -Details "$(Write-Output $csprojfile.Name):$(Write-Output $path)"
                }
            }
        }
    }
}

# function Example()
# {
#     param(
#         [string[]]$stringArray,
#         [switch]$switch,
#         [int]$int = 20,
#         [string]$env = $env:AV_API_KEY
#     )
#     DoStuff(stuff)
#     $returnValue
# }