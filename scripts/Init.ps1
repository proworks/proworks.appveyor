. $PSScriptRoot/CommonFunctions.ps1
# Put any commands here that need to be run in the init phase

if ($env:RunOctoPack -eq 'true') {
    # These are windows environment variables that OctoPack looks for
    Set-AppveyorBuildVariable -Name 'OctoPackPublishPackageToFileShare' -Value 'C:\octopacked' # Write package to a known directory
    Set-AppveyorBuildVariable -Name 'OctoPackEnforceAddingFiles' -Value 'true' # Also add files listed in csproj
}