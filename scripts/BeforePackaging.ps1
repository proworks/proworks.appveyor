. $PSScriptRoot/CommonFunctions.ps1

$apppath = [string]$env:WINDOWSZIP_CONTENTS_PATH

if ($apppath.Length -gt 0)
{
    Write-Output "Generating Windows ZIP from $apppath"
    $env:APPVEYOR_WAP_ARTIFACT_NAME = 'windowszip'
    $env:websitezip_deploy_website = 'false'
    $env:windowszip_deploy_app = 'true'

    $origdir = (Get-Location).Path
    Set-Location "$env:APPVEYOR_BUILD_FOLDER\$apppath"
    Compress-Archive -Path * -CompressionLevel Optimal -DestinationPath $env:APPVEYOR_BUILD_FOLDER\WindowsApp.zip
    Push-AppveyorArtifact $env:APPVEYOR_BUILD_FOLDER\WindowsApp.zip -FileName WindowsApp.zip -DeploymentName windowszip
    Set-Location "$origdir"
}

if ("$env:APPVEYOR_WAP_ARTIFACT_NAME" -eq "websitezip")
{
    $pubfiles = Get-ChildItem -Path $env:APPVEYOR_BUILD_FOLDER -Filter 'appveyor.pubxml' -Recurse
    if ($null -ne $pubfiles)
    {
        $zipfile = (Get-Content $pubfiles[0].FullName | Where-Object { $_ -match '<PackageFileName>' }) -replace '^.*<PackageFileName>([^<]+)</PackageFileName>.*$','$1'
        $deployRoot = Get-DeployRoot -ZipFile "$zipfile"

        $bdpfile = (New-TemporaryFile).FullName
        $dplfile = (New-TemporaryFile).FullName

        $scriptContent = Get-Content $PSScriptRoot/GeneralWebsite.before-deploy.ps1
        $scriptContent = $scriptContent -replace '@deployRoot@',"$deployRoot\"
        $scriptContent | Set-Content $bdpfile

        if ($env:BlueGreenDeployment -eq 'true' -or $env:RollingBlueGreen -eq 'true')
        {
            $scriptContent = Get-Content $PSScriptRoot/BlueGreenDeploy.beta.deploy.ps1
            $origdir = (Get-Location).Path
            $bgpfile = (New-TemporaryFile).FullName
            Remove-Item -Path $bgpfile -Force -Confirm:$false -ErrorAction SilentlyContinue
            New-Item $bgpfile -ItemType Directory
            Copy-Item "$PSScriptRoot\BlueGreenDeploy.prod.deploy.ps1" "$bgpfile\deploy.ps1"
            Set-Location "$bgpfile"
            Compress-Archive -Path 'deploy.ps1' -CompressionLevel Optimal -DestinationPath $env:APPVEYOR_BUILD_FOLDER\ProdDeployApp.zip
            Push-AppveyorArtifact $env:APPVEYOR_BUILD_FOLDER\ProdDeployApp.zip -FileName ProdDeployApp.zip -DeploymentName prodzip
            Set-Location "$origdir"
        }
        else
        {
            $scriptContent = Get-Content $PSScriptRoot/GeneralWebsite.deploy.ps1
        }
        $scriptContent = $scriptContent -replace '@deployRoot@',"$deployRoot\"
        $scriptContent | Set-Content $dplfile

        Add-DeployRootFile -ZipFile "$zipfile" -EntryPrefix "$deployRoot" -File "$bdpfile","$dplfile" -Name "before-deploy.ps1","deploy.ps1"
    }
}

if ("$env:APPVEYOR_WAP_ARTIFACT_NAME" -eq "aspnetcore" -and $null -ne $env:APPVEYOR_WAP_DIRECTORY -and "$env:APPVEYOR_WAP_DIRECTORY" -ne "")
{
    $destDir = "$env:APPVEYOR_BUILD_FOLDER\$env:APPVEYOR_WAP_DIRECTORY"
    Write-Output "Copying ASP.NET Core deploy files to $destDir"
    Copy-Item -Verbose -Path "$PSScriptRoot\AspNetCore.before-deploy.ps1" -Destination "$destDir\before-deploy.ps1" -Force
    Copy-Item -Verbose -Path "$PSScriptRoot\AspNetCore.deploy.ps1" -Destination "$destDir\deploy.ps1" -Force
}

if ($env:AZURE_CORE_ZIP_NAME -ne $null -and "$env:AZURE_CORE_ZIP_NAME" -ne "" -and $env:AZURE_CORE_ZIP_PROJECT_PATH -ne $null -and "$env:AZURE_CORE_ZIP_PROJECT_PATH" -ne "")
{
    Write-Output "Creating Azure ZIP package"
    Set-Location $env:APPVEYOR_BUILD_FOLDER
    mkdir artifacts
    mkdir package
    Set-Location $env:AZURE_CORE_ZIP_PROJECT_PATH
    dotnet publish -o "$env:APPVEYOR_BUILD_FOLDER\package" -c $env:CONFIGURATION
    Compress-Archive -Path "$env:APPVEYOR_BUILD_FOLDER\package\*" -DestinationPath "$env:APPVEYOR_BUILD_FOLDER\artifacts\${env:AZURE_CORE_ZIP_NAME}.zip"
    Push-AppveyorArtifact "$env:APPVEYOR_BUILD_FOLDER\artifacts\${env:AZURE_CORE_ZIP_NAME}.zip" -FileName $env:AZURE_CORE_ZIP_NAME
}

if ($null -ne $env:StaticPackageName -and "$env:StaticPackageName".Trim().Length -gt 0 -and $null -ne $env:StaticPackagePath -and "$env:StaticPackagePath".Trim().Length -gt 0 -and $null -ne $env:NpmBuildPath -and "$env:NpmBuildPath".Trim().Length -gt 0)
{
    Write-Output "Creating static files package"
    Set-Location $env:APPVEYOR_BUILD_FOLDER
    mkdir artifacts
    Set-Location "$env:APPVEYOR_BUILD_FOLDER\$env:NpmBuildPath"
    npm install --no-optional --no-progress --no-audit --loglevel=error;
    npm build
    if (!(Test-Path "$env:APPVEYOR_BUILD_FOLDER\$env:StaticPackagePath")) { throw "Could not generate the static package" }
    Compress-Archive -Path "$env:APPVEYOR_BUILD_FOLDER\$env:StaticPackagePath\*" -DestinationPath "$env:APPVEYOR_BUILD_FOLDER\artifacts\${env:StaticPackageName}.zip"
    Push-AppveyorArtifact "$env:APPVEYOR_BUILD_FOLDER\artifacts\${env:StaticPackageName}.zip" -FileName $env:StaticPackageName
}