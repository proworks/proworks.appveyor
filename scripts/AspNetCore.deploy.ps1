#Copy all DEPLOYENV_... environment variable into the web.config file at /configuration/location[path='.']/system.webServer/aspNetCore/environmentVariables

[xml]$webConfig = Get-Content -Path "$env:APPLICATION_PATH\web.config"
if ($null -ne $webConfig)
{
    $environmentVariables = $webConfig.SelectSingleNode("/configuration/location[@path='.']/system.webServer/aspNetCore/environmentVariables")
    $changes = 0

    #Set email environment defaults if they aren't already set
    if ($null -eq $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__Host -or $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__Host -eq "") {
        Write-Output 'Updating the email settings to the standard values'
        $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__From = 'noreply-iisdev@proworks.com'
        $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__Host = 'smtp.mailtrap.io'
        $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__Port = '25'
        $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__SecureSocketOptions = 'Auto'
        $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__DeliveryMethod = 'Network'
        $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__PickupDirectoryLocation = ''
        $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__Username = '1ad4656cdb8eb3'
        $env:DEPLOYENV_Umbraco__CMS__Global__Smtp__Password = '5156b9fc322054'
    } else {
        Write-Output 'Keeping the email settings in the current environment variables'
    }

    ForEach ($deployEnv in $(Get-ChildItem env:DEPLOYENV_*))
    {
        $EnvKey = $deployEnv.Key.Substring(10)
        $EnvValue = $deployEnv.Value

        $envNode = $environmentVariables.SelectSingleNode("environmentVariable[@name='$EnvKey']")
        $needsChange = $false

        if ($null -eq $envNode)
        {
            $envNode = $webConfig.CreateElement('environmentVariable')
            $envNode.SetAttribute('name', $EnvKey) | Out-Null
            $environmentVariables.AppendChild($envNode) | Out-Null
            $needsChange = $true
        }
        else
        {
            if ($envNode.value -ne $EnvValue)
            {
                $needsChange = $true
            }
        }

        if ($needsChange)
        {
            Write-Output "Setting $EnvKey variable"
            $envNode.SetAttribute('value', $EnvValue) | Out-Null
            $changes += 1
        }
        else
        {
            Write-Output "Keeping existing value for $EnvKey variable"
        }
    }

    if ($changes -gt 0)
    {
        Write-Output "Saving $changes environment variable updates to the web.config file"
        $webConfig.Save("$env:APPLICATION_PATH\web.config")
    }
    else
    {
        Write-Output 'No changes to the web.config file were needed'
    }
}
else
{
    Write-Output 'No web.config file found'
}

if ($null -ne $env:APPLICATION_SITE_NAME -and $env:APPLICATION_SITE_NAME -ne "")
{
    $WebSite = Get-Website -Name $env:APPLICATION_SITE_NAME
    if ($null -ne $WebSite)
    {
        $AppPool = $WebSite.applicationPool

        if ($null -ne $AppPool -and $AppPool -ne "")
        {
            Import-Module WebAdministration -ErrorAction SilentlyContinue
            $AppPoolState = (Get-WebAppPoolState -Name $AppPool).Value

            if ($AppPoolState -eq 'Stopped')
            {
                Start-WebAppPool -Name $AppPool

                $iterations = 0
                While (((Get-WebAppPoolState -Name $AppPool).Value) -ne 'Started' -and $iterations -le 90)
                {
                    Start-Sleep 1
                    $iterations += 1
                }
            }

            $AppPoolState = (Get-WebAppPoolState -Name $AppPool).Value
            if ($AppPoolState -ne 'Started')
            {
                Write-Output "Could not start the application pool $AppPool for site $env:APPLICATION_SITE_NAME, the final state was $AppPoolState"
            }
            else
            {
                Write-Output "Successfully started the application pool $AppPool for site $env:APPLICATION_SITE_NAME"
            }
        }
        else
        {
            Write-Output "Could not find an application pool for the website $env:APPLICATION_SITE_NAME"
        }
    }
    else
    {
        Write-Output "Could not find website $env:APPLICATION_SITE_NAME"
    }
}

$globalDeploy=($env:ProgramData + "\ProWorks\AppVeyor\Global-Deploy.ps1")

if (Test-Path $globalDeploy)
{
    Write-Output "Executing $globalDeploy"
    . $globalDeploy
}
else
{
    Write-Output "No global deploy file found at $globalDeploy"
}