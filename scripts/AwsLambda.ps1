function Deploy-NodeJsAwsLambda()
{
    param(
        [string]$BaseFolder,
        [string[]]$IncludedFiles,
        [string]$LambdaName,
        [string[]]$DeployToRegions
    )

    Set-Location "$BaseFolder"
    Write-Host 'Downloading all NodeJS dependencies'
    npm install

    $zipName = "$LambdaName.zip"
    $zipPath = "$env:APPVEYOR_BUILD_FOLDER\artifacts\$zipName"
    mkdir "$env:APPVEYOR_BUILD_FOLDER\artifacts"

    Write-Host "Compressing files into $zipName"
    7z a "$zipPath" @IncludedFiles node_modules

    Write-Host "Pushing artifact $zipName"
    Push-AppveyorArtifact "$zipPath" -FileName "$zipName" -DeploymentName "$LambdaName"

    foreach ($DeployToRegion in $DeployToRegions) {
        $env:AWS_DEFAULT_REGION = $DeployToRegion
        Write-Host "Deploying Lambda code to $LambdaName in the $DeployToRegion region"
        aws lambda update-function-code --function-name "$LambdaName" --zip-file "fileb://$zipPath" --publish
    }
}
