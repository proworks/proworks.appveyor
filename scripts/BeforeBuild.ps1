. $PSScriptRoot/CommonFunctions.ps1
. $PSScriptRoot/Tests.ps1

$origdir = (Get-Location).Path

# Run "npx gulp" for all gulpfile.js files
foreach ($gulpfile in (Find-MatchingFiles -FileName 'gulpfile.js' -RelativePaths "$env:BUILD_GULP_FOLDERS" -Depth "$env:BUILD_GULP_MAX_DEPTH"))
{
    Start-Job -Name ('GulpFile-' + $gulpfile.Directory.Name) -ScriptBlock {
        Set-Location $args[0].DirectoryName;
        npm install;
        npx gulp $args[1];
        if ($args[2].length -gt 1)
        {
            npx gulp $args[2]
        }
        elseif ((Get-Content $args[0].FullName -Raw) -match "gulp\.task\s*\(\s*'cache-bust'" -and "$env:BUILD_GULP_CACHE_BUST" -ne "false")
        {
            npx gulp cache-bust
        }
    } -ArgumentList @($gulpfile, $env:GulpTask, $env:GulpCacheBustTaskName) | Select-Object -Property 'Name'
}

# Run "npx gulp" for all gulpfile.mjs files
foreach ($gulpfile in (Find-MatchingFiles -FileName 'gulpfile.mjs' -RelativePaths "$env:BUILD_GULP_FOLDERS" -Depth "$env:BUILD_GULP_MAX_DEPTH"))
{
    Start-Job -Name ('GulpFile-' + $gulpfile.Directory.Name) -ScriptBlock {
        Set-Location $args[0].DirectoryName;
        npm install;
        npx gulp $args[1];
        if ($args[2].length -gt 1)
        {
            npx gulp $args[2]
        }
    } -ArgumentList @($gulpfile, $env:GulpTask) | Select-Object -Property 'Name'
}

# Run "npm run build" for all package.json files in a Vite project
if (-not [string]::IsNullOrEmpty($env:BUILD_VITE_FOLDERS)) {
	foreach ($packagefile in (Find-MatchingFiles -FileName 'package.json' -RelativePaths "$env:BUILD_VITE_FOLDERS" -Depth "$env:BUILD_VITE_MAX_DEPTH"))
	{
		Start-Job -Name ('Vite-' + $packagefile.Directory.Name) -ScriptBlock {
			Set-Location $args[0].DirectoryName;
			npm install;
            npm install sass@1.54.0;
			npm run build;
		} -ArgumentList @($packagefile) | Select-Object -Property 'Name'
	}
}

# Run "npx webpack -p" for all webpack.config.js files
foreach ($webpack in (Find-MatchingFiles -FileName 'webpack.config.js' -RelativePaths "$env:BUILD_WEBPACK_FOLDERS" -Depth "$env:BUILD_WEBPACK_MAX_DEPTH"))
{
    Start-Job -Name ('WebPack-' + $webpack.Directory.Name) -ScriptBlock {
        Set-Location $args[0].DirectoryName;
        npm install --no-optional --no-progress --no-audit --loglevel=error;
        npx webpack -p;
    } -ArgumentList @($webpack) | Select-Object -Property 'Name'
}

# Run "nuget restore" for all solutions
foreach ($slnfile in (Find-MatchingFiles -FileName '*.sln' -RelativePaths "$env:BUILD_NUGET_FOLDERS" -Depth "$env:BUILD_NUGET_MAX_DEPTH"))
{
    Start-Job -Name ('NuGet-' + $slnfile.Name) -ScriptBlock {
        Set-Location $args[0].DirectoryName;
        nuget restore $args[0].Name -verbosity quiet
    } -ArgumentList @($slnfile) | Select-Object -Property 'Name'
}

Get-Job | Wait-Job | Receive-Job

Check-Csproj-Included-Files
Check-AppData-Files

Set-Location $origdir