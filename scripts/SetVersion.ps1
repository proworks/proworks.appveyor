$bv = [string]$env:APPVEYOR_BUILD_VERSION
# We do this replace in case we aren't the first job in a set of jobs, otherwise the version number has already been transformed
$bv = ($bv -replace '([0-9]+\.[0-9]+\.[0-9]+)(-.*)?-r([0-9]+) .*', '$1.$3')
$tag_name = [string]$env:APPVEYOR_REPO_TAG_NAME
$version = [Version]$bv
$rev = $version.Revision.ToString()
$version_suffix = [string]$env:VersionSuffix
$expver = $version.Major.ToString() + "." + $version.Minor.ToString() + "." + $version.Build.ToString()

if ($env:IgnoreUmbracoVersion -ne 'true')
{
    $packagesConfigFiles = Get-ChildItem -Path $env:APPVEYOR_BUILD_FOLDER -Filter 'packages.config' -Recurse -Depth 2
    $umbracoVersionString = $null

    if ($null -ne $packagesConfigFiles) {
        foreach ($packagesConfig in $packagesConfigFiles) {
            foreach ($line in Get-Content $packagesConfig.FullName) {
                # Match lines like <package id="UmbracoCms" version="7.15.2 and gets the 3 numbers inside version
                if ($line -match '^\s*<package.*id="UmbracoCms".*version="(\d+)\.(\d+)\.(\d+)') {
                    if ($Matches[2].Length -eq 1) {
                        $Matches[2] = "0" + $Matches[2] # Versions like 7.1.1 will become 7011
                    }
                    $umbracoVersionString = $Matches[1] + $Matches[2] + $Matches[3]
                    Write-Output ("Found Umbraco version in " + $packagesConfig.FullName.Substring($env:APPVEYOR_BUILD_FOLDER.Length) + ": $umbracoVersionString")
                    $umbracoVersionMatch = ($umbracoVersionString -eq $version.Major.ToString())
                }
            }
        }
    }

    if ($null -eq $umbracoVersionString) {
        $csprojFiles = Get-ChildItem -Path $env:APPVEYOR_BUILD_FOLDER -Filter '*.csproj' -Recurse -Depth 2
        if ($null -ne $csprojFiles) {
            foreach ($csprojFile in $csprojFiles) {
                foreach ($line in Get-Content $csprojFile.FullName) {
                    # Match lines like <package id="UmbracoCms" version="7.15.2 and gets the 3 numbers inside version
                    if ($line -match '^\s*<PackageReference.*Include="Umbraco\.Cms".*Version="(\d+)\.(\d+)\.(\d+)') {
                        if ($Matches[2].Length -eq 1) {
                            $Matches[2] = "0" + $Matches[2] # Versions like 7.1.1 will become 7011
                        }
                        $umbracoVersionString = $Matches[1] + $Matches[2] + $Matches[3]
                        Write-Output ("Found Umbraco version in " + $csprojFile.FullName.Substring($env:APPVEYOR_BUILD_FOLDER.Length) + ": $umbracoVersionString")
                        $umbracoVersionMatch = ($umbracoVersionString -eq $version.Major.ToString())
                    }
                }
            }
        }

        if ($null -eq $umbracoVersionString) {
            if ($null -ne $packagesConfigFiles -or $null -ne $csprojFiles) {
                Write-Warning "Could not find Umbraco version in any of the following files:"
                if ($null -ne $packagesConfigFiles) {
                    Write-Warning "$($packagesConfigFiles | ForEach-Object FullName)"
                }
                if ($null -ne $csprojFiles) {
                    Write-Warning "$($csprojFile | ForEach-Object FullName)"
                }
                Write-Warning "Please make sure Umbraco is installed with NuGet package manager"
            } else {
                Write-Warning "Could not find packages.config to verify Umbraco version"
                Write-Warning "Please make sure Umbraco is installed with NuGet package manager"
            }
        }
    }

    if ($null -eq $env:UmbracoVersionVerification) {
        $umbracoVersionMatch = $true # This causes all checks to pass
    }
} else {
    $umbracoVersionMatch = $true # This causes all checks to pass
}

# Match tags like v7152.1.0 (v123.4.5)
if ($env:APPVEYOR_REPO_TAG -eq 'true' -and $tag_name -match '^v(\d+)\.(\d+)\.(\d+)$') {
    $basever = $Matches[1] + "." + $Matches[2] + "." + $Matches[3]
    if ($env:IgnoreYmlVersion -eq 'true') {
        $expver = $basever
    }
    # Use numbers in tag as version
    if ($expver -eq $basever -and $umbracoVersionMatch) {
        Write-Warning "THIS IS A TAGGED RELEASE BUILD: $($basever.ToString())"
        Set-AppveyorBuildVariable -Name PROJECT_NEXT_VERSION -Value ($version.Major.ToString() + "." + $version.Minor.ToString() + "." + ($version.Build + 1).ToString())
        $suffixedver = "$basever"
    }
    else {
        Write-Warning "INVALID VERSION BUILD TAG"
        Write-Warning "Tagged Version:    $($basever.ToString())"
        Write-Warning "Version in yml:    $expver"
        Write-Warning "Umbraco Version:   $umbracoVersionString"
        $host.SetShouldExit(1)
        Exit
    }
}

# Match branches like release/7152/1.0 (release/123/4.5)
elseif ($env:APPVEYOR_REPO_BRANCH -match '^release/') {
    $env:APPVEYOR_REPO_BRANCH -match '^release\/(\d+)\/(\d+)\.(\d+)$'
    # Use numbers in branch name as version
    $basever = $Matches[1] + "." + $Matches[2] + "." + $Matches[3]

    if ($expver -eq $basever -and $umbracoVersionMatch) {
        Write-Warning "THIS IS A RELEASE BRANCH BUILD:  $basever"
        Set-AppveyorBuildVariable -Name PROJECT_NEXT_VERSION -Value ($version.Major.ToString() + "." + $version.Minor.ToString() + "." + ($version.Build + 1).ToString())
        $suffixedver = "$basever-Production"
    }
    else {
        Write-Warning "INVALID VERSION IN YML FILE OR BRANCH NAME"
        Write-Warning "Branch Name:       $env:APPVEYOR_REPO_BRANCH"
        Write-Warning "Branch Version:    $basever"
        Write-Warning "Version in yml:    $expver"
        Write-Warning "Umbraco Version:   $umbracoVersionString"
        $host.SetShouldExit(1)
        Exit
    }
}

# Anything that is not a release (Dev and Authoring)
else {
    if ($null -ne $umbracoVersionString) {
        if ($umbracoVersionString -ne $version.Major.ToString()) {
            Write-Warning "Version in yml file does not match the detected Umbraco version"
            Write-Warning "Version in yml:   $expver"
            Write-Warning "Umbraco Version:  $umbracoVersionString"
        }
        $basever = $umbracoVersionString + "." + $version.Minor.ToString() + "." + $version.Build.ToString()
    }
    else {
        $basever = $expver
    }
    if ($version_suffix.Trim().Length -gt 0) {
        $suffixedver = "$basever-$version_suffix"
    }
    else {
        $suffixedver = $basever
    }

}

$env:BASEVER = $basever
$env:SIMPLEVER = $basever + "." + $rev
$env:SEMVER = $suffixedver
$env:APPVEYOR_VERSION = "$suffixedver-r$rev (" + (Get-Date).ToString('yyMMdd_HHmmss') + ")"
Write-Output "ENVIRONMENT: BASEVER =  $env:BASEVER"
Write-Output "ENVIRONMENT: SIMPLEVER = $env:SIMPLEVER"
Write-Output "ENVIRONMENT: SEMVER =  $env:SEMVER"
Write-Output "ENVIRONMENT: APPVEYOR_VERSION =  $env:APPVEYOR_VERSION"
Update-AppveyorBuild -Version $env:APPVEYOR_VERSION

$nuspecFiles = Get-ChildItem -Filter '*.nuspec' -Recurse -Depth 3
if ($null -ne $nuspecFiles) {
    $nuspecPaths = ($nuspecFiles).FullName | Where-Object { !$_.Contains('\packages\') }
    $commitMsg = [string]$env:APPVEYOR_REPO_COMMIT_MESSAGE
    if ($null -ne $nuspecPaths) {
        foreach ($fl in $nuspecPaths) {
            $contents = Get-Content $fl -Raw
            $ptn = [System.Text.RegularExpressions.Regex]'<version>[^<]*</version>'
            $contents = $ptn.Replace($contents, "<version>" + $env:SEMVER + "</version>")

            if ($null -ne $commitMsg -and $commitMsg.Trim().Length -gt 0) {
                $ptn = [System.Text.RegularExpressions.Regex]'<releaseNotes>[^<]*</releaseNotes>'
                $contents = $ptn.Replace($contents, "<releaseNotes>" + $commitMsg + "</releaseNotes>")
            }

            $contents | Set-Content $fl
            Write-Output "Updated NuSpec file $fl"
        }
    }
}

$assemblyFiles = Get-ChildItem -Filter 'AssemblyInfo.cs' -Recurse -Depth 3
if ($null -ne $assemblyFiles) {
    $assemblyPaths = ($assemblyFiles).FullName | Where-Object { !$_.Contains('\packages\') }
    if ($null -ne $assemblyPaths) {
        $shortBaseVerTail = ''
        try
        {
            $shortBaseVerTail = $env:BASEVER.SubString($env:BASEVER.IndexOf('.') + 1)
        }
        catch
        {
            Write-Warning "Unable to determine minor.build.revision version from $env:BASEVER"
        }
        
        foreach ($fl in $assemblyPaths) {
            # Regex for locations in file
            $ptnA = [System.Text.RegularExpressions.Regex]'AssemblyVersion\("(?<Version>[^"]*)"\)'
            $ptnF = [System.Text.RegularExpressions.Regex]'AssemblyFileVersion\("[^"]*"\)'
            $ptnI = [System.Text.RegularExpressions.Regex]'AssemblyInformationalVersion\("[^"]*"\)'
            
            # Determine new AssemblyVersion
            $contents = (Get-Content $fl -Raw)
            $existingVersion = $ptnA.Match($contents).Groups['Version'].Value
            $newVersion = $existingVersion
            if($shortBaseVerTail.length -gt 0 -and $shortBaseVerTail.Split('.').length -eq 4) {
                $newVersion = $existingVersion.Split('.')[0] + '.' + $shortBaseVerTail
            }
            #Write-Output "contents: $contents"
            Write-Output "shortBaseVerTail: $shortBaseVerTail"
            Write-Output "existingVersion: $existingVersion"
            Write-Output "newVersion: $newVersion"
            
            # Format assembly versions for file
            $verA = 'AssemblyVersion("' + $newVersion + '")'
            $verF = 'AssemblyFileVersion("' + $env:SIMPLEVER + '")'
            $verI = 'AssemblyInformationalVersion("' + $env:SEMVER + ' Commit ' + $env:APPVEYOR_REPO_COMMIT + '")'
            
            $contents = $ptnA.Replace($contents, $verA)
            if ($ptnF.IsMatch($contents)) {
                $contents = $ptnF.Replace($contents, $verF)
            }
            else {
                $contents = $ptnA.Replace($contents, '$0]' + "`r`n" + '[assembly: ' + $verF)
            }
            if ($ptnI.IsMatch($contents)) {
                $contents = $ptnI.Replace($contents, $verI)
            }
            else {
                $contents = $ptnF.Replace($contents, '$0]' + "`r`n" + '[assembly: ' + $verI)
            }

            $contents | Set-Content $fl
            Write-Output "Updated Assembly Information file $fl"
        }
    }
}