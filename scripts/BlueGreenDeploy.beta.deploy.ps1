$BetaSiteName = [string]$env:BlueGreen_BetaSiteName
if ($null -eq $BetaSiteName -or $BetaSiteName -eq '') { $BetaSiteName = $env:APPLICATION_SITE_NAME }
if ($null -eq $BetaSiteName -or $BetaSiteName -eq '') { throw 'Neither the BlueGreen_BetaSiteName environment variable nor the APPLICATION_SITE_NAME environment variable are not set'; Exit 1 }

Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Starting Blue/Green Beta deploy for ' + $BetaSiteName)

# The name of the production site.  We will pull what database it is currently using from here, and get the connection string to set in the beta site's web.config file
$ProductionSiteName = [string]$env:BlueGreen_ProductionSiteName

# A connection string to use to connect to the database in order to backup the current production DB and restore it into a second DB.
# The server name in this connection string is also used to validate that the production site's DB connection string points to the same server
$DbConnection = [string]$env:BlueGreen_DbConnection

# The path on the file system of the DB server where the backup should be written to.  Also the default path on the local server where archived backups or logs are placed
$DbBackupPath = [string]$env:BlueGreen_DbBackupPath

if ($null -eq $ProductionSiteName -or $ProductionSiteName -eq '') { throw 'The BlueGreen_ProductionSiteName environment variable is not set'; Exit 1 }
if ($null -eq $DbConnection -or $DbConnection -eq '') { throw 'The BlueGreen_DbConnection environment variable is not set'; Exit 1 }
if ($null -eq $DbBackupPath -or $DbBackupPath -eq '') { throw 'The BlueGreen_DbBackupPath environment variable is not set'; Exit 1 }

$SkipCopyFolders = (([string]$env:BlueGreen_Skip_Copy_Folders) -eq "true")
$SkipBackupProductionDb = (([string]$env:BlueGreen_Skip_Backup_Production_Db) -eq "true")
$SkipRestoreBetaDb = (([string]$env:BlueGreen_Skip_Restore_Beta_Db) -eq "true")
$SkipWarmBetaSite = (([string]$env:BlueGreen_Skip_Warm_Beta_Site) -eq "true")
$DeleteOldDirectories = (([string]$env:BlueGreen_Delete_Directories) -eq "true")
$DeleteOldDatabases = (([string]$env:BlueGreen_Delete_Databases) -eq "true")
$ArchiveOldDbBackups = (([string]$env:BlueGreen_Archive_Db_Backups) -eq "true")
$ArchiveOldIisLogs = (([string]$env:BlueGreen_Archive_Iis_Logs) -eq "true")
$OldDbRetention = [int]$env:BlueGreen_Retain_Old_Database_Count
$OldDirRetention = [int]$env:BlueGreen_Retain_Old_Directory_Count
$DbBackupFile = [string]$env:BlueGreen_DbBackupFile
$DbDataFileName = [string]$env:BlueGreen_DbDataFileName
$DbDataFileFolder = [string]$env:BlueGreen_DbDataFileFolder
$DbLogFileName = [string]$env:BlueGreen_DbLogFileName
$DbLogFileFolder = [string]$env:BlueGreen_DbLogFileFolder

$ArchiveFolder = [string]$env:BlueGreen_ArchiveFolder
if ($null -eq $ArchiveFolder -or $ArchiveFolder -eq '') { $ArchiveFolder = $DbBackupPath }

$IgnoredSitePattern = [string]$env:BlueGreen_IgnoredSitePattern
if ($null -eq $IgnoredSitePattern -or $IgnoredSitePattern.Trim().Length -eq 0) { $IgnoredSitePattern = $null }

$IgnoredSites = $env:BlueGreen_IgnoredSites -split ','
if ($null -eq $IgnoredSites -or $IgnoredSites.Length -eq 0 -or ( $IgnoredSites.Length -eq 1 -and ( $null -eq $IgnoredSites[0] -or $IgnoredSites[0] -eq '') ) ) { $IgnoredSites = @() }

$CopiedFolders = $env:BlueGreen_CopiedFolders -split ','
if ($null -eq $CopiedFolders -or $CopiedFolders.Length -eq 0 -or ( $CopiedFolders.Length -eq 1 -and ( $null -eq $CopiedFolders[0] -or $CopiedFolders[0] -eq '') ) ) { $CopiedFolders = @('media','App_Data\UmbracoForms','App_Data\packages') }

$DbServer = [string]$env:BlueGreen_DbServer
if ($null -eq $DbServer -or $DbServer -eq '') { $DbServer = (([string]($DbConnection -split ';' | Where-Object { $_ -imatch '^\s*(Server|Data Source)\s*=' })).Trim() -split '=')[1] }
if ($null -eq $DbServer -or $DbServer -eq '') { throw 'The BlueGreen_DbServer environment variable is not set, and the BlueGreen_DbConnection environment variable does not contain a server reference'; Exit 1 }

Import-Module WebAdministration
$BetaSitePath = (Get-Website -Name $BetaSiteName).PhysicalPath
if ($null -eq $BetaSitePath -or $BetaSitePath -eq '' -or $BetaSitePath.Count -gt 1) { $BetaSitePath = (Get-Website | Where-Object { $_.Name -eq $BetaSiteName }).PhysicalPath }
$BetaFolderName = [System.IO.Path]::GetFileName($BetaSitePath)

$SiteBasePath = [string]$env:BlueGreen_SiteBasePath
if ($null -eq $SiteBasePath -or $SiteBasePath -eq '') { $SiteBasePath = [System.IO.Path]::GetDirectoryName($BetaSitePath) }
if ($null -eq $SiteBasePath -or $SiteBasePath -eq '') { throw 'The BlueGreen_SiteBasePath environment variable is not set'; Exit 1 }

$SiteNamePattern = [string]$env:BlueGreen_SiteNamePattern
if (($null -eq $SiteNamePattern -or $SiteNamePattern -eq '') -and $BetaFolderName -match '^.+_[0-9]{8}_[0-9]{6}$') { $SiteNamePattern = ($BetaFolderName -replace '^(.+)_[0-9]{8}_[0-9]{6}$','$1') + '_[0-9]{8}_[0-9]{6}$' }
if ($null -eq $SiteNamePattern -or $SiteNamePattern -eq '') { throw 'The BlueGreen_SiteNamePattern environment variable is not set'; Exit 1 }

$DeployDate = [string]$env:BlueGreen_DeployDate
if (($null -eq $DeployDate -or $DeployDate -eq '') -and $BetaFolderName -match '^.+_[0-9]{8}_[0-9]{6}$') { $DeployDate = $BetaFolderName -replace '^.+_([0-9]{8}_[0-9]{6})$','$1' }
if ($null -eq $DeployDate -or $DeployDate -eq '') { throw 'The BlueGreen_DeployDate environment variable is not set'; Exit 1 }

$DbNamePrefix = [string]$env:BlueGreen_DbNamePrefix
if (($null -eq $DbNamePrefix -or $DbNamePrefix -eq '') -and $BetaFolderName -match '^.+_[0-9]{8}_[0-9]{6}$') { $DbNamePrefix = $BetaFolderName -replace '^(.+)_[0-9]{8}_[0-9]{6}$','$1' }
if ($null -eq $DbNamePrefix -or $DbNamePrefix -eq '') { throw 'The BlueGreen_DbNamePrefix environment variable is not set'; Exit 1 }

$Sites = Get-Website
$SitePaths = ($Sites).PhysicalPath
$SiteDBs = @()
$ProductionDb = ''
$ProductionSitePath = ''
$BetaDb = "${DbNamePrefix}_$DeployDate"
$BetaConn = ''
$DbNamePattern = '^' + $DbNamePrefix + '_[0-9]{8}_[0-9]{6}$'
$HadError = $false

Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Finding information on existing sites')
ForEach ($Site in $Sites) {
	$SiteName = $Site.Name
	$SitePath = $Site.PhysicalPath
	$SiteConfig = "$SitePath\Web.config"

	if ($IgnoredSites -icontains $SiteName -or ($null -ne $IgnoredSitePattern -and $SiteName -imatch $IgnoredSitePattern)) {
		continue
	}
	if (Test-Path $SiteConfig) {
		$xml = [xml](Get-Content $SiteConfig)
		$dbKey = $xml.SelectSingleNode("/configuration/connectionStrings/add[@name='umbracoDbDSN']")

		if ($null -ne $dbKey) {
			$dbConn = [string]$dbKey.GetAttribute("connectionString")

			if ($null -ne $dbConn -and $dbConn.Trim().Length -gt 0) {
				$dbConnParts = $dbConn -split ';'
				$serverPart = ([string]($dbConnParts | Where-Object { $_ -imatch '^\s*(Server|Data Source)\s*=' })).Trim()
				$databasePart = ([string]($dbConnParts | Where-Object { $_ -imatch '^\s*(DataBase|Initial Catalog)\s*=' })).Trim()

				if ($null -ne $serverPart -and $null -ne $databasePart) {
					$serverVal = [string](($serverPart -split '=')[1])
					$databaseVal = [string](($databasePart -split '=')[1])

					if ($serverVal -ieq "$DbServer" -and $null -ne $databaseVal -and $databaseVal.Trim().Length) {
						$SiteDBs += $databaseVal
						if ($SiteName -ieq $ProductionSiteName) {
							$ProductionDb = $databaseVal
                            $ProductionSitePath = $SitePath
                            $cnt = 0
                            while ($cnt -lt $dbConnParts.Length) {
                                if ($dbConnParts[$cnt] -imatch '^\s*(DataBase|Initial Catalog)\s*=') {
                                    $dbConnParts[$cnt] = ($dbConnParts[$cnt] -split '=')[0] + '=' + $BetaDb
                                }
                                $cnt = $cnt + 1
                            }
                            $BetaConn = $dbConnParts -join ';'
                        }
					} else {
						Write-Output "ERROR: The umbracoDbDSN connection string has a server of '$serverVal' and a database value of '$databaseVal' which were not the expected '$DbServer'.'*' for '$SiteName' in the file $SiteConfig"
						$HadError = $true
					}
				} else {
					Write-Output "ERROR: The umbracoDbDSN connection string did not have a server or database specified for '$SiteName' in the file $SiteConfig"
					$HadError = $true
				}
			} else {
				Write-Output "ERROR: The umbracoDbDSN connection string was empty for '$SiteName' in the file $SiteConfig"
				$HadError = $true
			}
		} else {
			Write-Output "ERROR: Could not find the umbracoDbDSN connection string for '$SiteName' in the file $SiteConfig"
			$HadError = $true
		}
	} else {
		Write-Output "ERROR: Could not find the site config file for '$SiteName' at $SiteConfig"
		$HadError = $true
	}
}

if ($HadError) {
	throw "There were one or more errors in validating the websites.  Please see the errors above.  Either correct the errors, or add the site to the list of ignored sites"
	Exit 1
}

if ($null -eq $ProductionDb -or $ProductionDb.Trim().Length -eq 0) {
	throw "The production database name was not found for site $ProductionSiteName"
	Exit 1
}

if ($null -eq $BetaSitePath -or $BetaSitePath.Trim().Length -eq 0) {
	throw "The beta site path was not found for site $BetaSiteName"
	Exit 1
}

if (!(Test-Path "$BetaSitePath\web.config")) {
	throw "The beta site config file was not found for site $BetaSiteName at $BetaSitePath\web.config"
	Exit 1
}

if ($BetaDb -ieq $ProductionDb) {
	throw "The beta database name of $BetaDb for site $BetaSiteName matches the production database name in site $ProductionSiteName"
	Exit 1
}



Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Connecting to the database and finding the existing databases and files')
$Conn = New-Object System.Data.SqlClient.SqlConnection $DbConnection
$Conn.Open()
if ($Conn.State -ne 'Open') {
	$Conn.Dispose()
	throw "Could not open a connection to the database"
	Exit 1
}
if ($null -eq $DbDataFileName -or $DbDataFileName.Trim().Length -eq 0 -or $null -eq $DbDataFileFolder -or $DbDataFileFolder.Trim().Length -eq 0 -or $null -eq $DbLogFileName -or $DbLogFileName.Trim().Length -eq 0 -or $null -eq $DbLogFileFolder -or $DbLogFileFolder.Trim().Length -eq 0) {
    $Cmd = New-Object System.Data.SqlClient.SqlCommand "USE [$ProductionDb]; SELECT type_desc, name, physical_name FROM sys.database_files",$Conn
    $Rdr = $Cmd.ExecuteReader()
    While ($Rdr.Read()) {
        $DbType = $Rdr.GetString(0)
        $DbName = $Rdr.GetString(1)
        $DbPath = $Rdr.GetString(2)

        if ($DbType -eq 'ROWS') {
            if ($DbDataFileName -or $DbDataFileName.Trim().Length -eq 0) { $DbDataFileName = "$DbName" }
            if ($DbDataFileFolder -or $DbDataFileFolder.Trim().Length -eq 0) { $DbDataFileFolder = [System.IO.Path]::GetDirectoryName("$DbPath") }
        } elseif ($DbType -eq 'LOG') {
            if ($DbLogFileName -or $DbLogFileName.Trim().Length -eq 0) { $DbLogFileName = "$DbName" }
            if ($DbLogFileFolder -or $DbLogFileFolder.Trim().Length -eq 0) { $DbLogFileFolder = [System.IO.Path]::GetDirectoryName("$DbPath") }
        }
    }
    $Rdr.Dispose()
    $Cmd.Dispose()

    if ($null -eq $DbDataFileName -or $DbDataFileName.Trim().Length -eq 0 -or $null -eq $DbDataFileFolder -or $DbDataFileFolder.Trim().Length -eq 0 -or $null -eq $DbLogFileName -or $DbLogFileName.Trim().Length -eq 0 -or $null -eq $DbLogFileFolder -or $DbLogFileFolder.Trim().Length -eq 0) {
        $Conn.Dispose()
        throw "Could not determine the database file locations"
        Exit 1
    }
}

$DbDeletes = @()
$SkippedDbs = 0
$Cmd = New-Object System.Data.SqlClient.SqlCommand 'SELECT name FROM sys.databases ORDER BY 1 DESC',$Conn
$Rdr = $Cmd.ExecuteReader()
While ($Rdr.Read()) {
	$DbName = $Rdr.GetString(0)
	if ($DbName -imatch $DbNamePattern -and $SiteDBs -inotcontains $DbName) {
        if ($SkippedDbs -ge $OldDbRetention) {
            $DbDeletes += $DbName
        } else {
            $SkippedDbs = $SkippedDbs + 1
        }
	}
}
$Rdr.Dispose()
$Cmd.Dispose()

ForEach ($CopiedFolder in $CopiedFolders)
{
    $SrcFolder = "$ProductionSitePath\$CopiedFolder"
    $DstFolder = "$BetaSitePath\$CopiedFolder"
    if ($SkipCopyFolders) {
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Skipping copying $CopiedFolder from production to beta:  $SrcFolder   -->   $DstFolder")
    } else {
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Copying $CopiedFolder from production to beta:  $SrcFolder   -->   $DstFolder")
        if (!(Test-Path $DstFolder)) { New-Item -ItemType Directory "$DstFolder" | Out-Null }
        Copy-Item -Path "$SrcFolder\*" -Destination "$DstFolder" -Recurse -ErrorAction SilentlyContinue
    }
}

if ($null -eq $DbBackupFile -or $DbBackupFile.Trim().Length -eq 0) {
    $CurDate = (Get-Date).ToString('yyyy-MM-dd_HHmmss')
    $BackupFile = "$DbBackupPath\${ProductionDb}_-_${CurDate}.bak"
} else {
    $BackupFile = $DbBackupFile
}

if ($SkipBackupProductionDb) {
	Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Skipping backing up production database $ProductionDb to $BackupFile because of an environment variable")
} else {
	if ($DbServer -ne 'localhost' -or !(Test-Path $BackupFile)) {
		Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Backing up production database $ProductionDb to $BackupFile")
		$CmdText = "BACKUP DATABASE [$ProductionDb] TO DISK = N'$BackupFile' WITH NOFORMAT, NOINIT, NAME = N'$ProductionDb-Full Database Backup', SKIP, NOREWIND, NOUNLOAD, STATS = 10"
		Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -     Executing: $CmdText")
		$Cmd = New-Object System.Data.SqlClient.SqlCommand $CmdText,$Conn
		$Cmd.CommandTimeout = 3600
		$BackupResult = $Cmd.ExecuteNonQuery()
		$Cmd.Dispose()
		if ($BackupResult -ne -1) {
			$Conn.Dispose()
			throw "Could not backup the existing production database"
			Exit 1
		}
	} else {
		Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Skipping backing up production database $ProductionDb to $BackupFile because the backup file already exists")
	}
}

if ($SkipRestoreBetaDb) {
	Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Skipping restoring production database to $BetaDb because of an environment variable")
} else {
    if ($null -ne $BackupFile -and $BackupFile.Trim().Length -gt 0 -and ($DbServer -ne 'localhost' -or (Test-Path $BackupFile))) {
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Restoring production database to $BetaDb")
        $CmdText = "RESTORE DATABASE [$BetaDb] FROM DISK = N'$BackupFile' WITH FILE = 1, MOVE N'$DbDataFileName' TO N'$DbDataFileFolder\$BetaDb.mdf', MOVE N'$DbLogFileName' TO N'$DbLogFileFolder\$BetaDb.ldf', NOUNLOAD, STATS = 5"
        Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -     Executing: $CmdText")
        $Cmd = New-Object System.Data.SqlClient.SqlCommand $CmdText,$Conn
        $Cmd.CommandTimeout = 300
        $RestoreResult = $Cmd.ExecuteNonQuery()
        $Cmd.Dispose()
        if ($RestoreResult -ne -1) {
            $Conn.Dispose()
            throw "Could not restore the existing production database"
            Exit 1
        }
    } else {
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Skipping restoring production database to $BetaDb because the backup file $BackupFile doesn't exist")
    }
}

$Conn.Dispose()

Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Updating the database connection string for the beta site')
$xml = [xml](Get-Content "$BetaSitePath\web.config")
$dbKey = $xml.SelectSingleNode("/configuration/connectionStrings/add[@name='umbracoDbDSN']")
$dbKey.SetAttribute('connectionString', "$BetaConn")
$xml.Save("$BetaSitePath\web.config")

if ($SkipWarmBetaSite) {
    Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + ' - Skipping warming call to the beta site')
} else {
    $bindings = (Get-WebBinding -Name $BetaSiteName | Where-Object { $_.protocol -eq 'http' })
    if ($bindings.Length -eq 0) {
        $bindings = (Get-WebBinding -Name $BetaSiteName | Where-Object { $_.protocol -eq 'https' })
    }
    if ($null -eq $bindings.Length -or $bindings.Length -eq 1) {
        $binding = $bindings
    } elseif ($bindings.Length -gt 1) {
        $binding = $bindings[0]
    }
    
    if ($null -ne $binding) {
        try
        {
            $bindingParts = $binding.bindingInformation -split ':'
            $bindingPort = $bindingParts[1]
            $bindingHost = $bindingParts[2]
            if ($bindingParts[0] -eq '*') {
                $bindingServer = $bindingHost
            } else {
                $bindingServer = $bindingParts[0]
            }
    
            $scheme = $binding.protocol
            $betaUrl = "${scheme}://${bindingServer}:${bindingPort}/umbraco/"
            Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Attempting to invoke the beta site at $betaUrl with a Host header of $bindingHost")
    
            $BetaAppPool = Get-ItemProperty -Path "IIS:\Sites\$BetaSiteName" -Name 'applicationPool'
            Start-Website -Name $BetaSiteName -ErrorAction SilentlyContinue
            Start-WebAppPool -Name $BetaAppPool -ErrorAction SilentlyContinue
    
            $resp = Invoke-WebRequest -Uri "$betaUrl" -Headers @{"Host"=$bindingHost}
            Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -     Got a status code of " + $resp.StatusCode)
        }
        catch {
            $dt = Get-Date -Format 'yyyy-MM-dd HH:mm:ss'
            Write-Output '',"$dt -     WARNING: Could not successfully invoke the beta site.  You will need to manually access the site.","$dt -         $_",("$dt -         " + $_.ScriptStackTrace)
        }
    } else {
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - WARNING: Could not find a suitable binding to attemp to invoke the beta site at.  You will need to manually access the site.")
    }
}

$globalDeploy=($env:ProgramData + "\ProWorks\AppVeyor\Global-Deploy.ps1")

if (Test-Path $globalDeploy)
{
    Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Executing $globalDeploy")
    . $globalDeploy
}
else
{
    Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - No global deploy file found at $globalDeploy")
}

$FinishDt = Get-Date -Format 'yyyy-MM-dd HH:mm:ss'
$StarLine = "$FinishDt - **********************************************************************************************"
$SuccessL = "$FinishDt - **   Successfully completed setting up the Beta site $BetaSiteName for a Blue/Green swap with production   **"
$StarHole = "$FinishDt - **                                                                                            "
$DoneLine = "$FinishDt - **   All further log entries are for cleanup and do not prevent access to the beta site       "
$CharCnt = 0
while ($CharCnt -lt $BetaSiteName.Length)
{
	$StarLine += '*'
	$StarHole += ' '
	$DoneLine += ' '
	$CharCnt += 1
}
$StarLine += '**'
$StarHole += '**'
$DoneLine += '**'
Write-Output '','','','','','',"$StarLine","$StarLine","$StarHole","$SuccessL"
if ($DeleteOldDirectories -or $DeleteOldDatabases -or $ArchiveOldDbBackups -or $ArchiveOldIisLogs) { Write-Output "$StarHole","$DoneLine" }
Write-Output "$StarHole","$StarLine","$StarLine",'','','','','',''

if ($DeleteOldDirectories) {
	Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Deleting old site directories")
	$ToDeletes = ((Get-ChildItem -Path "$SiteBasePath" | Where-Object { $_.Name -imatch "$SiteNamePattern" }).FullName | Where-Object { $SitePaths -inotcontains $_ } | Sort-Object -Property 'Name' -Descending)
	if ($null -ne $ToDeletes -and $ToDeletes.Count -gt $OldDirRetention) {
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Deleting old site directories")
        $SkippedDirs = 0
		ForEach ($ToDelete in $ToDeletes) {
            if ($SkippedDirs -ge $OldDirRetention) {
                Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -     Deleting path $ToDelete")
                Remove-Item $ToDelete -Force -Confirm:$false -Recurse
            } else {
                $SkippedDirs = $SkippedDirs + 1
            }
		}
	} else {
		Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - No old site directories to delete")
	}
}

if ($DeleteOldDatabases) {
	Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Deleting old databases")
	if ($null -ne $DbDeletes -and $DbDeletes.Count -gt 0) {
		Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Deleting old databases")
		ForEach ($DbDelete in $DbDeletes) {
            Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -     Deleting $DbDelete")
            $Cmd = $null
            try {
                $CmdText = "EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'$DbDelete'"
                Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -         Executing: $CmdText")
                $Cmd = New-Object System.Data.SqlClient.SqlCommand $CmdText,$Conn
                $Cmd.ExecuteNonQuery()
            } catch {
                Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -         Could not delete the backup history, continuing")
            }
            if ($null -ne $Cmd) { $Cmd.Dispose() }

			$CmdText = "DROP DATABASE [$DbDelete]"
			Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -         Executing: $CmdText")
			$Cmd = New-Object System.Data.SqlClient.SqlCommand $CmdText,$Conn
			$Cmd.ExecuteNonQuery()
			$Cmd.Dispose()
		}
	} else {
		Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - No old databases to delete")
	}
}

if ($ArchiveOldDbBackups) {
	Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Compressing and removing backup files in $ArchiveFolder")
	$BackupFiles = (Get-ChildItem -Path "$ArchiveFolder" -Filter "*.bak").FullName
	if ($null -ne $BackupFiles -and $BackupFiles.Count -gt 0) {
		Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Compressing and removing backup files in $ArchiveFolder")
		ForEach ($BackupFile in $BackupFiles) {
			Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -     Compressing $BackupFile")
			& "C:\Program Files\7-Zip\7z.exe" a -t7z "$BackupFile.7z" "$BackupFile" -mx9 -mmt -y
			if ($LastExitCode -eq 0) {
				Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -         Deleting $BackupFile")
				Remove-Item $BackupFile -Force -Confirm:$false
			}
		}
	} else {
		Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - No backup files to compress and remove in $ArchiveFolder")
	}
}

if ($ArchiveOldIisLogs) {
	Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Compressing and removing old IIS logs")
	$CutoffDate = (Get-Date).AddMonths(-1)
	$CutoffString = $CutoffDate.ToString('yyyy-MM-dd_HHmmss')

    if (Test-Path C:\inetpub\logs\LogFiles) {
        $LogSources = (Get-ChildItem -Path C:\inetpub\logs\LogFiles -Directory).Name
        ForEach ($LogSource in $LogSources) {
            $SrcDir = "C:\inetpub\logs\LogFiles\$LogSource"
            $IisLogs = (Get-ChildItem -Path "$SrcDir" -Filter 'u_ex*.log' | Where-Object { $_.LastWriteTime -lt $CutoffDate }).FullName
            if ($null -ne $IisLogs -and $IisLogs.Count -gt 0) {
                $DestFile = "$ArchiveFolder\${LogSource}-Logs-Before-${CutoffString}.7z"
                $Cnt = $IisLogs.Count
                Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - Compressing $Cnt old IIS logs from $SrcDir to $DestFile")
                & "C:\Program Files\7-Zip\7z.exe" a -t7z "$DestFile" $IisLogs -mx9 -mmt -y -m0=PPMd
                if ($LastExitCode -eq 0) {
                    Write-Output ((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " -         Deleting old logs")
                    Remove-Item $IisLogs -Force -Confirm:$false
                }
            } else {
                Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - No old IIS logs in $SrcDir to compress and remove")
            }
        }
    } else {
        Write-Output '',((Get-Date -Format 'yyyy-MM-dd HH:mm:ss') + " - No accessible IIS logs")
    }
}
