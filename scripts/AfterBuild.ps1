. $PSScriptRoot/CommonFunctions.ps1

# Find NuGet packages, push to Octopus, create release and deploy
$octokey = [string]$env:OctopusApiKey
$octourl = [string]$env:OctopusUrl
$octoproj = [string]$env:OctopusProject
$octochan = [string]$env:OctopusChannel
$octoenv = [string]$env:OctopusEnvironment

if ($env:RunOctoPack -eq 'true' -and $octokey.Length -gt 0 -and $octourl.Length -gt 0 -and $octoproj.Length -gt 0 -and $octochan.Length -gt 0 -and $octoenv.Length -gt 0)
{
    $NuGetPackages = @(Get-ChildItem C:\octopacked -File -Filter *.nupkg)
    ForEach ($NuGetPackage in $NuGetPackages) {
      Push-AppveyorArtifact $NuGetPackage.FullName -FileName $NuGetPackage.Name
      NuGet push $NuGetPackage.FullName -ApiKey "$octokey" -Source "https://$octourl/nuget/packages"
    }

    octo create-release --server "https://$octourl/" -apiKey "$octokey" --project="$octoproj" --channel="$octochan" --deployto="$octoenv" --releaseNumber="$($env:SIMPLEVER)" --progress
}

if ($env:Package_AspNet_Core_Deploy_Files -eq "true") {
  $publishPath = (Get-ChildItem -Path $env:TEMP -Filter 'Umbraco.Core.dll' -Depth 1).Directory.FullName
  if ($null -ne $publishPath -and $publishPath.Trim().Length -gt 0) {
    Write-Output "Copying ASP.NET Core deploy files to $publishPath"
    Copy-Item -Verbose -Path "$PSScriptRoot\AspNetCore.before-deploy.ps1" -Destination "$publishPath\before-deploy.ps1" -Force
    Copy-Item -Verbose -Path "$PSScriptRoot\AspNetCore.deploy.ps1" -Destination "$publishPath\deploy.ps1" -Force
  } else {
    Write-Warning ('Could not find the publish path under ' + $env:TEMP)
  }
}